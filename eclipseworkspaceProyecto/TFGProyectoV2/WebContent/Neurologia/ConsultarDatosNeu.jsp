<%--
  Created by IntelliJ IDEA.
  User: Diego
  Date: 15/04/2021
  Time: 12:27
  To change this template use File | Settings | File Templates.
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="ISO-8859-1">
    <meta charset="UTF-8">
    <title>Clínica Mompo</title>

    <link rel="stylesheet" type="text/css" href="estilo/estiloTexto.css">
    <link rel="stylesheet" type="text/css" href="estilo/estilo.css">
    <script src="js/Neurologia/consultarDatosNeu.js" type="text/javascript"></script> <!-- Archivo javascript para consultar los datos del paciente-->
    <script src="js/jquery.min.js"></script> <!-- Archivo javascript de jQuery-->
</head>
<body>
    <c:forEach var="con" items="${sessionScope.conNeu}">
     <p>Datos del paciente</p>
    <p>Nombre: ${con.nombre}</p>
    <p>Primer apellido: ${con.apellido1}</p>
        <p>Segundo apellido: ${con.apellido2}</p>
        <p>Peso: ${con.peso}</p>
        <p>Altura: ${con.altura}</p>
    </c:forEach>
</body>
</html>
