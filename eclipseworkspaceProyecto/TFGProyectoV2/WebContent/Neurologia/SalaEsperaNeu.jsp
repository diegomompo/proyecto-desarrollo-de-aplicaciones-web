<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>

	<meta charset="ISO-8859-1">
	<meta charset="UTF-8">
	<title>Cl�nica Mompo</title>

	<link rel="stylesheet" type="text/css" href="estilo/estiloTexto.css">
	<link rel="stylesheet" type="text/css" href="estilo/estilo.css">
	<script src="js/Neurologia/salaEsperaNeu.js" type="text/javascript"></script> <!-- Archivo javascript de la sala de Espera-->
	<script src="js/jquery.min.js"></script> <!-- Archivo javascript de jQuery-->
</head>
<body>
		<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>

	<p>El paciente ${sessionScope.avisaNombre}
		${sessionScope.avisaApellido} ${sessionScope.avisaApellido2} ya puede entrar a la consulta</p>
	
</body>
</html>