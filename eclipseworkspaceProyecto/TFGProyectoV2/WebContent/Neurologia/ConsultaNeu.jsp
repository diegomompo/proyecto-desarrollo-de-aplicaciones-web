<%--
  Created by IntelliJ IDEA.
  User: Diego
  Date: 14/04/2021
  Time: 12:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="ISO-8859-1">
    <meta charset="UTF-8">
    <title>Clínica Mompo</title>

    <link rel="stylesheet" type="text/css" href="estilo/estiloTexto.css">
    <link rel="stylesheet" type="text/css" href="estilo/estilo.css">
    <script src="js/Neurologia/consultaNeu.js" type="text/javascript"></script> <!-- Archivo javascript de consulta de Neurologia -->
    <script src="js/jquery.min.js"></script> <!-- Archivo javascript de jQuery-->
</head>
<body>
    <%String msj = (String)request.getAttribute("msj");
    if(msj != null){%>
        <p><%=msj %></p>
    <%} %>

    <p>Paciente: ${sessionScope.avisaNombre} ${sessionScope.avisaApellido} ${sessionScope.avisaApellido2}</p>
    <p>Medico: ${sessionScope.iMed[0]}</p>

</body>
</html>
