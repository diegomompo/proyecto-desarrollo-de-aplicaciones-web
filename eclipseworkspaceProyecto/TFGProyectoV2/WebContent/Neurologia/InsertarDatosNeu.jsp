<%--
  Created by IntelliJ IDEA.
  User: Diego
  Date: 15/04/2021
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="ISO-8859-1">
    <meta charset="UTF-8">
    <title>Clínica Mompo</title>

    <link rel="stylesheet" type="text/css" href="estilo/estiloTexto.css">
    <link rel="stylesheet" type="text/css" href="estilo/estiloForm.css">
    <script src="js/Neurologia/insertarDatosNeu.js" type="text/javascript"></script> <!-- Archivo javascript para insertar los datos del paciente-->
    <script src="js/jquery.min.js"></script> <!-- Archivo javascript de jQuery-->
</head>
<body>
    <p>Insertar datos para el paciente ${sessionScope.avisaNombre}
        ${sessionScope.avisaApellido} ${sessionScope.avisaApellido2} </p>
</body>
</html>
