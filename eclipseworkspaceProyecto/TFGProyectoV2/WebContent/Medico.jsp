<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

	<meta charset="ISO-8859-1">
	<meta charset="UTF-8">
	<title>Cl�nica Mompo</title>

	<link rel="stylesheet" type="text/css" href="estilo/estiloTexto.css">
	<link rel="stylesheet" type="text/css" href="estilo/estiloForm.css">
	<script src="js/jquery.min.js"></script> <!-- Archivo javascript de jQuery-->
</head>
<body>
		<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>

		<h1>SOLICITAR TURNO: Escoger medico</h1>

		<form action="Main?op=seleccionandoMedico" method="post">

			<select name="medico">
				<option>Selecciona un medico}</option>
				<option value="1">${sessionScope.sMedico[0]}</option>
				<option value="3">${sessionScope.sMedico[1]}</option>
				<option value="4">${sessionScope.sMedico[2]}</option>
				<option value="5">${sessionScope.sMedico[3]}</option>
				<option value="9">${sessionScope.sMedico[4]}</option>
				<option value="11">${sessionScope.sMedico[5]}</option>
				<option value="13">${sessionScope.sMedico[6]}</option>


			</select>

			<button type="submit" class="btn btn-primary">Seleccionar Medico</button>
		</form>

		<table>
		<tr>
			<td style="width: 960px">
				<p class="encabezadoAmarillo">EN NUESTRA CLINICA USTED ESCOGE SU MEDICO</p>
				<p class="cuerpoBlanco">Disponemos de los mejores profesionales. El paciente escoje el medico que mas le gusta.</p>
			</td>
			<td  style="width: 960px">
				<img src="img/ImagenMedico.png" class="imgInst">
			</td>
		</tr>
		</table>

</body>
</html>