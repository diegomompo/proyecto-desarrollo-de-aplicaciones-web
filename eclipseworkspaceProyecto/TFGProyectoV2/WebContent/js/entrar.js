window.onload = function(){
	entrarClinica();
}
function entrarClinica(){
	let divEntrar = $("<div>");
	divEntrar.attr("id", "ent");
	
	$("body").append(divEntrar);
	
	jsonEntrar();
}

//LLAMADA A JSON
function jsonEntrar(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/entrar.json",
		"aysnc": true,
		}
	).done(entrandoClinica)
}
function entrandoClinica(objetoJSONEntrar){
	arrayEntrar = objetoJSONEntrar.entrando;
	arrayTurno = objetoJSONEntrar.solicitarTurno;
	arraySalaEspera = objetoJSONEntrar.irSalaEspera;
	
	
	$.each(arrayEntrar, function(i, entrando){
		$.each(arrayTurno, function(i, solicitarTurno){
			$.each(arraySalaEspera, function(i, irSalaEspera){

				//TITULO
				let elementh1 = $(entrando.elementH1);
				elementh1.text(entrando.titleH1);

				//BOTON PARA SOLICITAR EL TURNO
				let elementFormTurno = $(entrando.elementForm);
				elementFormTurno.attr("action", solicitarTurno.actionFormTurno);
				elementFormTurno.attr("method", entrando.method);
				let elementButtonTurno = $(entrando.elementButton);
				elementFormTurno.addClass("boton1");
				elementButtonTurno.attr("type", entrando.typeButton);
				elementButtonTurno.attr("class", entrando.classButton);
				elementButtonTurno.text(solicitarTurno.titleButton);

				//BOTON PARA IR A LA SALA DE ESPERA
				let elementFormEspera = $(entrando.elementForm);
				elementFormEspera.attr("action", irSalaEspera.actionFormEspera);
				elementFormEspera.attr("method", entrando.method);
				let elementButtonEspera = $(entrando.elementButton);
				elementButtonEspera.attr("type", entrando.typeButton);
				elementButtonEspera.attr("class", entrando.classButton);
				elementButtonEspera.text(irSalaEspera.titleButton);

				// TABLA DE INSTRUCCIONES
				var tabla = document.createElement("TABLE");
				var filas = document.createElement("TR");
				var columna1 = document.createElement("TD");

				var tituloP1 = document.createElement("p");
				var titulo1 = document.createTextNode("SOLICITAR TURNO");
				var textoP1 = document.createElement("p");
				var texto1 = document.createTextNode("La persona de recepcion recibe al paciente y le asigas un turno para la consulta");

				var tituloP2 = document.createElement("p");
				var titulo2 = document.createTextNode("COMPROBAR TURNO");
				var textoP2 = document.createElement("p");
				var texto2 = document.createTextNode("Para comprobar si el paciente tenia ya turno pedido.");

				columna1.style.width="960px";
				tituloP1.className = "encabezadoAmarillo";
				textoP1.className = "cuerpoBlanco";
				tituloP2.className = "encabezadoAmarillo";
				textoP2.className = "cuerpoBlanco";

				var columna2 = document.createElement("TD");

				var img = document.createElement("IMG");
				img.setAttribute("src", "img/ImagenInicio.png");

				columna2.style.width="960px";
				img.className = "imgInst";

				//IMPRIMIR PARA MOSTRARLO EN JSP
				$("#ent").append(elementh1);
				$("#ent").append(elementFormTurno);
				$(elementFormTurno).append(elementButtonTurno);
				$("#ent").append(elementFormEspera);
				$(elementFormEspera).append(elementButtonEspera);

				//IMPRIMIR TABLA DE INSTRUCCIONES
				document.body.appendChild(tabla);
				tabla.appendChild(filas);
				//document.body.appendChild(filas);
				filas.appendChild(columna1);
				columna1.appendChild(tituloP1);
				tituloP1.appendChild(titulo1);
				columna1.appendChild(textoP1);
				textoP1.appendChild(texto1);
				columna1.appendChild(tituloP2);
				tituloP2.appendChild(titulo2);
				columna1.appendChild(textoP2);
				textoP2.appendChild(texto2);
				filas.appendChild(columna2);
				columna2.appendChild(img);

			})
		})
	})
}

/**
 * 
 */S