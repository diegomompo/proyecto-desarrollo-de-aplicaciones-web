window.onload = function (){
    $(document).ready(entrarConsultaNeu);
}
function entrarConsultaNeu(){
    let divEConsulta = $("<div>");
    divEConsulta.attr("id", "entrarNeu");
    $("body").append(divEConsulta);

    entrarNeuJSON();
}
function entrarNeuJSON() {
    $.ajax({
            'type': 'GET',
            'url': "./json/Neurologia/consultaNeu.json",
            "async": true
        }
    ).done(eConsultaNeu);
}
    function eConsultaNeu(objetoJSONEntrar) {
        '<%String msj = (String)request.getAttribute("msj");'
        'if(msj != null){%>'
        '<p><%=msj %></p>'
        '<%} %>'

        arrayConsultaNeu = objetoJSONEntrar.conNeu;
        arrayInsertar = objetoJSONEntrar.irInsertarDatos;
        arrayConsultar = objetoJSONEntrar.irConsultarDatos;
        arrayTerminar = objetoJSONEntrar.irTerminarConsulta;
        arrayVolver = objetoJSONEntrar.volverInicio;

        $.each(arrayConsultaNeu, function (i, conNeu) {
            $.each(arrayInsertar, function (i, irInsertarDatos) {
                $.each(arrayConsultar, function (i, irConsultarDatos) {
                    $.each(arrayTerminar, function (i, irTerminarConsulta) {
                        $.each(arrayVolver, function (i, volverInicio) {

                            //TITULO
                            let elementH1 = $(conNeu.elementh1);
                            elementH1.text(conNeu.titleh1);

                            //FORMULARIO BOTON INSERTAR DATOS PACIENTE
                            let elementFormInsertar = $(conNeu.elementForm);
                            elementFormInsertar.attr("action", irInsertarDatos.actionForm);
                            elementFormInsertar.attr("method", conNeu.methodForm);

                            //BOT�N PARA INSERTAR LOS DATOS DEL PACIENTE
                            let elementButtonInsertar = $(conNeu.elementButton);
                            elementButtonInsertar.attr("type", conNeu.typeButton);
                            elementButtonInsertar.attr("class", conNeu.classButton);
                            elementButtonInsertar.text(irInsertarDatos.titleButton);

                            //FORMULARIO BOTON CONSULTAR DATOS PACIENTE
                            let elementFormConsultar = $(conNeu.elementForm);
                            elementFormConsultar.attr("action", irConsultarDatos.actionForm);
                            elementFormConsultar.attr("method", conNeu.methodForm);

                            //BOT�N PARA CONSULTAR LOS DATOS DEL PACIENTE
                            let elementButtonConsultar = $(conNeu.elementButton);
                            elementButtonConsultar.attr("type", conNeu.typeButton);
                            elementButtonConsultar.attr("class", conNeu.classButton);
                            elementButtonConsultar.text(irConsultarDatos.titleButton);

                            //FORMULARIO BOTON TERMINAR CONSULTA
                            let elementFormTerminar = $(conNeu.elementForm);
                            elementFormTerminar.attr("action", irTerminarConsulta.actionForm);
                            elementFormTerminar.attr("method", conNeu.methodForm);

                            //BOT�N PARA TERMINAR CONSULTA
                            let elementButtonTerminar = $(conNeu.elementButton);
                            elementButtonTerminar.attr("type", conNeu.typeButton);
                            elementButtonTerminar.attr("class", conNeu.classButton);
                            elementButtonTerminar.text(irTerminarConsulta.titleButton);

                            //FORMULARIO PARA BOTON DE VOLVER AL INICIO
                            let elementFormVolver = $(conNeu.elementForm);
                            elementFormVolver.attr("action", volverInicio.actionFormVolver);
                            elementFormVolver.attr("method", conNeu.methodForm);

                            //BOT�N PARA VOLVER AL INICIO
                            let elementButtonVolver = $(conNeu.elementButton);
                            elementButtonVolver.attr("type", conNeu.typeButton);
                            elementButtonVolver.attr("class", conNeu.classButton);
                            elementButtonVolver.text(volverInicio.titleButton);

                            // TABLA DE INSTRUCCIONES
                            var tabla = document.createElement("TABLE");
                            var filas = document.createElement("TR");
                            var columna1 = document.createElement("TD");

                            var tituloP1 = document.createElement("p");
                            var titulo1 = document.createTextNode("INSERTAR/CONSULTAR DATOS");
                            var textoP1 = document.createElement("p");
                            var texto1 = document.createTextNode("A todos los pacientes se les mide y se les pesa en cada consulta. Con estos dos botones se insertan los datos o se consultan.");

                            var tituloP2 = document.createElement("p");
                            var titulo2 = document.createTextNode("TERMINAR LA CONSULTA");
                            var textoP2 = document.createElement("p");
                            var texto2 = document.createTextNode("Pulsar este boton cuando la consulta haya terminado, para indicar que el medico esta libre para recibir al siguiente.");

                            var tituloP3 = document.createElement("p");
                            var titulo3 = document.createTextNode("VOLVER AL INICIO");
                            var textoP3 = document.createElement("p");
                            var texto3 = document.createTextNode("Este boton es para volver al menu principal en el caso que llegue un nuevo paciente al que hay darle turno, o si se quiere validar a un paciente que ha llegado a la clinica.");

                            columna1.style.width="960px";
                            tituloP1.className = "encabezadoAmarillo";
                            textoP1.className = "cuerpoBlanco";
                            tituloP2.className = "encabezadoAmarillo";
                            textoP2.className = "cuerpoBlanco";
                            tituloP3.className = "encabezadoAmarillo";
                            textoP3.className = "cuerpoBlanco";

                            var columna2 = document.createElement("TD");

                            var img = document.createElement("IMG");
                            img.setAttribute("src", "img/ImagenConsulta.png");

                            columna2.style.width="960px";
                            img.className = "imgInst";

                            //IMPRIMIR TITULO POR PANTALLA
                            $("#entrarNeu").append(elementH1);

                            //IMPRIMIR BOTON INSERTAR DATOS DE PACIENTE POR PANTALLA
                            $("#entrarNeu").append(elementFormInsertar);
                            $(elementFormInsertar).append(elementButtonInsertar);

                            //IMPRIMIR BOTON CONSULTAR DATOS DE PACIENTE POR PANTALLA
                            $("#entrarNeu").append(elementFormConsultar);
                            $(elementFormConsultar).append(elementButtonConsultar);

                            //IMPRIMIR BOTON TERMINAR CONSULTA
                            $("#entrarNeu").append(elementFormTerminar);
                            $(elementFormTerminar).append(elementButtonTerminar);

                            //IMPRIMIR BOTON VOLVER A INICIO
                            $("#entrarNeu").append(elementFormVolver);
                            $(elementFormVolver).append(elementButtonVolver);

                            //IMPRIMIR TABLA DE INSTRUCCIONES
                            document.body.appendChild(tabla);
                            tabla.appendChild(filas);
                            filas.appendChild(columna1);
                            columna1.appendChild(tituloP1);
                            tituloP1.appendChild(titulo1);
                            columna1.appendChild(textoP1);
                            textoP1.appendChild(texto1);
                            columna1.appendChild(tituloP2);
                            tituloP2.appendChild(titulo2);
                            columna1.appendChild(textoP2);
                            textoP2.appendChild(texto2);
                            columna1.appendChild(tituloP3);
                            tituloP3.appendChild(titulo3);
                            columna1.appendChild(textoP3);
                            textoP3.appendChild(texto3);
                            filas.appendChild(columna2);
                            columna2.appendChild(img);
                        })
                    })
                })
            })
        })
    }
