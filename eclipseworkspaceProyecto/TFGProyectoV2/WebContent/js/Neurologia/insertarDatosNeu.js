window.onload = function () {
    $("document").ready(insertNeu);
}
function insertNeu() {
   let divInsertNeu = $("<div>");
   divInsertNeu.attr("id", "ins");
   $("body").append(divInsertNeu);

   insertNeuJSON();
}
function insertNeuJSON() {
    $.ajax({
            'type': 'GET',
            'url': "./json/Neurologia/insertNeu.json",
            "async": true
        }
    ).done(insertDatosNeu);
}
function insertDatosNeu(objetoJSONInsertarNeu) {

    arrayInsNeu = objetoJSONInsertarNeu.insDatosNeu;
    arrayPesoNeu = objetoJSONInsertarNeu.datosPesoNeu;
    arrayAturaNeu = objetoJSONInsertarNeu.datosAlturaNeu;

    // DECLARAMOS LAS FUNCIONES JSON
    $.each(arrayInsNeu, function (i, insDatosNeu) {
    $.each(arrayPesoNeu, function (i, datosPesoNeu) {
    $.each(arrayAturaNeu, function (i, datosAlturaNeu) {

        //TITULO
        let elementH1 = $(insDatosNeu.elementh1);
        elementH1.text(insDatosNeu.titleh1);

        //FORMULARIO PARA ENVIAR LA PETICI�N AL SERVLET
        let elementFORM = $(insDatosNeu.elementForm);
        elementFORM.attr("action", insDatosNeu.actionForm);
        elementFORM.attr("method", insDatosNeu.methodForm);


        //CAMPO PESO
        let elementDivPeso = $(insDatosNeu.elementDiv);
        elementDivPeso.attr("class", insDatosNeu.classDiv);

        let elementLabelPeso = $(insDatosNeu.elementLabel);
        elementLabelPeso.attr("for", datosPesoNeu.forPeso);
        elementLabelPeso.text(datosPesoNeu.titlePeso);

        let elementInputPeso = $(insDatosNeu.elementInput);
        elementInputPeso.attr("type", insDatosNeu.typeInputD);
        elementInputPeso.attr("name", datosPesoNeu.namePeso);
        elementInputPeso.attr("class", insDatosNeu.classInput);
        elementInputPeso.attr("id", datosPesoNeu.idPeso);

        //CAMPO ALTURA
        let elementDivAltura = $(insDatosNeu.elementDiv);
        elementDivAltura.attr("class", insDatosNeu.classDiv);

        let elementLabelAltura = $(insDatosNeu.elementLabel);
        elementLabelAltura.attr("for", datosAlturaNeu.forAltura);
        elementLabelAltura.text(datosAlturaNeu.titleAltura);

        let elementInputAltura = $(insDatosNeu.elementInput);
        elementInputAltura.attr("type", insDatosNeu.typeInputD);
        elementInputAltura.attr("name", datosAlturaNeu.nameAltura);
        elementInputAltura.attr("class", insDatosNeu.classInput);
        elementInputAltura.attr("id", datosAlturaNeu.idAltura);

        //BOTON PARA INSERTAR DATOS
        let elementButton = $(insDatosNeu.elementButton);
        elementButton.attr("type", insDatosNeu.typeButton);
        elementButton.attr("class", insDatosNeu.classButton);
        elementButton.text(insDatosNeu.titleButton);

        // TABLA DE INSTRUCCIONES
        var tabla = document.createElement("TABLE");
        var filas = document.createElement("TR");
        var columna1 = document.createElement("TD");

        var tituloP1 = document.createElement("p");
        var titulo1 = document.createTextNode("INSERTAR DATOS");
        var textoP1 = document.createElement("p");
        var texto1 = document.createTextNode("Rellenar los nuevos datos de PESO y ALTURA");

        columna1.style.width="960px";
        tituloP1.className = "encabezadoAmarillo";
        textoP1.className = "cuerpoBlanco";

        var columna2 = document.createElement("TD");

        var img = document.createElement("IMG");
        img.setAttribute("src", "img/ImagenDatos.png");

        columna2.style.width="960px";
        img.className = "imgInst";

        //IMPRIMIR TITULO POR PANTALLA
        $("#ins").append(elementH1);

        //IMPRIMIR FORMULARIO POR PANTALLA
        $("#ins").append(elementFORM);

        //IMPRIMIR CAMPO PESO POR PANTALLA
        $(elementFORM).append(elementDivPeso);
        $(elementDivPeso).append(elementLabelPeso);
        $(elementDivPeso).append(elementInputPeso);

        //IMPRIMIR CAMPO PESO POR PANTALLA
        $(elementFORM).append(elementDivAltura);
        $(elementDivAltura).append(elementLabelAltura);
        $(elementDivAltura).append(elementInputAltura);

        //IMPRIMIR BOT�N INSERTAR DATOS
        $(elementFORM).append(elementButton);

        //IMPRIMIR TABLA DE INSTRUCCIONES
        document.body.appendChild(tabla);
        tabla.appendChild(filas);
        filas.appendChild(columna1);
        columna1.appendChild(tituloP1);
        tituloP1.appendChild(titulo1);
        columna1.appendChild(textoP1);
        textoP1.appendChild(texto1);
        filas.appendChild(columna2);
        columna2.appendChild(img);

    })
    })
    })
}
