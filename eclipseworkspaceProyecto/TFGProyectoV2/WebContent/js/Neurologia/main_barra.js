// BARRA de IR A LA SALA DE ESPERA

function barraYendoSalaEsperaNeu(){

    var header3 = $("<h1>");
    header3.text("Yendo a la sala de espera de Neurología");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra_Neurologia?op=barraIrSalaEsperaNeu");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();
}

//-------------------------------------------------------------------------------------------------

// BARRA de PREPARAR EL FORMULARIO PARA LLAMAR AL PACIENTE

function barraPreparandoLlamar(){

    var header3 = $("<h1>");
    header3.text("Preparando formulario para llamar al paciente");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Neurologia?op=llamandoPacienteNeu");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();
}
//-------------------------------------------------------------------------------------------------

// BARRA de LLAMANDO AL PACIENTE

function barraLlamandoPaciente(){

    var header3 = $("<h1>");
    header3.text("Llamando al paciente");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra_Neurologia?op=barraLlamarPaciente");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();
}
//-------------------------------------------------------------------------------------------------

// BARRA de ENTRAR A LA CONSULTA

function barraEntrandoConsulta(){

    var header3 = $("<h1>");
    header3.text("Entrando a la consulta");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Neurologia?op=entrandoConsultaNeu");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();
}
//-------------------------------------------------------------------------------------------------

// BARRA de PREPARARA EL FORMULARIO PARA INSERTAR LOS DATOS

function barraPrepararFormInsert(){

    var header3 = $("<h1>");
    header3.text("Preparando Formulario para insertar los datos");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Neurologia?op=yendoInsertarDatosPacienteNeu");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------

// BARRA de INSERTAR LOS DATOS DE UN PACIENTE

function barraInsertDatosPaciente(){

    var header3 = $("<h1>");
    header3.text("Insertando los datos del paciente");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra_Neurologia?op=barraInsertandoDatosPac");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------

// BARRA de CONSULTAR LOS DATOS DE UN PACIENTE

function barraConDatosPaciente(){

    var header3 = $("<h1>");
    header3.text("Consultando Datos del paciente");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Neurologia?op=consultandoDatosPaciente");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------

// BARRA de TERMINAR LA CONSULTA

function barraTerminandoConsulta(){

    var header3 = $("<h1>");
    header3.text("Terminado la consulta");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main?op=terminandoConsulta");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------

// BARRA de PREPARARA EL FORMULARIO PARA ACTUALIZAR LOS DATOS

function barraPrepararFormUpdate(){

    var header3 = $("<h1>");
    header3.text("Preparando Formulario para actualizar los datos");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Neurologia?op=yendoModificarDatosPacienteNeu");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------

// BARRA de INSERTAR LOS DATOS DE UN PACIENTE

function barraUpdateDatosPaciente(){

    var header3 = $("<h1>");
    header3.text("Actualizando los datos del paciente");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra_Neurologia?op=barraActualizarDatosPac");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------
// BARRA de TERMINAR LA CONSULTA

function barraVolviendoConsulta(){

    var header3 = $("<h1>");
    header3.text("Volviendo a la consulta");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra_Neurologia?op=barraVolviendoConsulta");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();

}
//-------------------------------------------------------------------------------------------------
// FUNCION QUE DEFINE EL CUERPO DE LA BARRA Y EK BOTON DE CERRAR LA BARRA

    function cuerpoBarra() {
        var fondo = $("<div>");
        fondo.attr("class", "estiloFondo");
        fondo.attr("id", "eFondo");

        var eBarra = $("<div>");
        eBarra.attr("class", "estiloBarra");
        eBarra.attr("id", "barra");

        var ePorcentaje = $("<div>");
        ePorcentaje.attr("class", "estiloPorcentaje");
        ePorcentaje.attr("id", "porcentaje");
        ePorcentaje.text("0%");

        $("body").append(fondo);
        $("#eFondo").append(eBarra);
        $("#barra").append(ePorcentaje);
    }

//--------------------------------------------------------------------------------------------------------

//FUNCION PARA EL CERRAR EL BOTON

    function cerrarButton() {
        var botonCerrar = $("<button>");

        botonCerrar.attr("type", "submit");
        botonCerrar.attr("class", "btn btn-warning");
        botonCerrar.text("Cerrar");

        $("#ir").append(botonCerrar);
    }