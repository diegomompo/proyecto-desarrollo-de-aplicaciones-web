window.onload = function(){
	$(document).ready(esperaSala);
}
function esperaSala() {
	let divEspera = $("<div>");
	divEspera.attr("id", "esp");
	
	$("body").append(divEspera);
	
	jsonEspera();
}

// LLAMADA A JSON
function jsonEspera(){
	$.ajax({
		'type' : 'GET',
		'url' : "./json/Neurologia/salaEsperaNeu.json",
		"aysnc": true,
	}).done(esperandoSala);
}

function esperandoSala(objetoJSONEspera){
	arrayEspera = objetoJSONEspera.espSala;
	arrayLlamar = objetoJSONEspera.llamarPac;
	arrayEntrar = objetoJSONEspera.entrarCon;
	arrayVolver = objetoJSONEspera.volverInicio;
	
	$.each(arrayEspera, function(i, espSala) {
		$.each(arrayLlamar, function (i, llamarPac) {
			$.each(arrayEntrar, function (i, entrarCon) {
				$.each(arrayVolver, function (i, volverInicio) {

					//TÍTULO
					let elementH1 = $(espSala.elementh1);
					elementH1.text(espSala.titleh1);

					//BOTON LLAMAR AL PACIENTE
					let elementFormLlamarP = $(espSala.elementForm);
					elementFormLlamarP.attr("action", llamarPac.actionFormLlamarP);
					elementFormLlamarP.attr("method", espSala.method);
					let elementButtonLlamarP = $(espSala.elementButton);
					elementButtonLlamarP.attr("type", espSala.typeButton);
					elementButtonLlamarP.attr("class", espSala.classButton);
					elementButtonLlamarP.text(llamarPac.titleButton);

					//BOTON ENTRAR A LA CONSULTA
					let elementFormEntrarC = $(espSala.elementForm);
					elementFormEntrarC.attr("action", entrarCon.actionFormEntrarC);
					elementFormEntrarC.attr("method", espSala.method);
					let elementButtonEntrarC = $(espSala.elementButton);
					elementButtonEntrarC.attr("type", espSala.typeButton);
					elementButtonEntrarC.attr("class", espSala.classButton);
					elementButtonEntrarC.text(entrarCon.titleButton);

					//BOTON ENTRAR A LA CONSULTA
					let elementFormVolver = $(espSala.elementForm);
					elementFormVolver.attr("action", volverInicio.actionFormVolver);
					elementFormVolver.attr("method", espSala.method);
					let elementButtonVolver = $(espSala.elementButton);
					elementButtonVolver.attr("type", espSala.typeButton);
					elementButtonVolver.attr("class", espSala.classButton);
					elementButtonVolver.text(volverInicio.titleButton);

					// TABLA DE INSTRUCCIONES
					var tabla = document.createElement("TABLE");
					var filas = document.createElement("TR");
					var columna1 = document.createElement("TD");

					var tituloP1 = document.createElement("p");
					var titulo1 = document.createTextNode("LLAMAR AL PACIENTE");
					var textoP1 = document.createElement("p");
					var texto1 = document.createTextNode("Pulsar este boton para llamar al paciente. No se puede entrar en la consulta si antes el paciente no ha sido llamado.\n");

					var tituloP2 = document.createElement("p");
					var titulo2 = document.createTextNode("ENTRAR A LA CONSULTA");
					var textoP2 = document.createElement("p");
					var texto2 = document.createTextNode("Cuando en esta pantalla aparezca el nombre y los apellidos del paciente, ya puede pasar. Si la persona no entra a la consulta, se llama a otro paciente.\n");

					var tituloP3 = document.createElement("p");
					var titulo3 = document.createTextNode("VOLVER AL INICIO");
					var textoP3 = document.createElement("p");
					var texto3 = document.createTextNode("Este boton es para volver al menu principal en el caso que llegue un nuevo paciente al que hay darle turno, o si se quiere validar a un paciente que ha llegado a la clinica.\n");

					columna1.style.width="960px";
					tituloP1.className = "encabezadoAmarillo";
					textoP1.className = "cuerpoBlanco";
					tituloP2.className = "encabezadoAmarillo";
					textoP2.className = "cuerpoBlanco";
					tituloP3.className = "encabezadoAmarillo";
					textoP3.className = "cuerpoBlanco";

					var columna2 = document.createElement("TD");

					var img = document.createElement("IMG");
					img.setAttribute("src", "img/ImagenMenuEspera.png");
					columna2.style.width="960px";
					img.className = "imgInst";

					//IMPRIMIR PARA MOSTRARLO EN EL JSP
					$("#esp").append(elementH1);

					$("#esp").append(elementFormLlamarP);
					$(elementFormLlamarP).append(elementButtonLlamarP);

					$("#esp").append(elementFormEntrarC);
					$(elementFormEntrarC).append(elementButtonEntrarC);

					$("#esp").append(elementFormVolver);
					$(elementFormVolver).append(elementButtonVolver);

					//IMPRIMIR TABLA DE INSTRUCCIONES
					document.body.appendChild(tabla);
					tabla.appendChild(filas);
					filas.appendChild(columna1);
					columna1.appendChild(tituloP1);
					tituloP1.appendChild(titulo1);
					columna1.appendChild(textoP1);
					textoP1.appendChild(texto1);
					columna1.appendChild(tituloP2);
					tituloP2.appendChild(titulo2);
					columna1.appendChild(textoP2);
					textoP2.appendChild(texto2);
					columna1.appendChild(tituloP3);
					tituloP3.appendChild(titulo3);
					columna1.appendChild(textoP3);
					textoP3.appendChild(texto3);
					filas.appendChild(columna2);
					columna2.appendChild(img);

				})
			})
		})
	})
	
}
/**
 * 
 */