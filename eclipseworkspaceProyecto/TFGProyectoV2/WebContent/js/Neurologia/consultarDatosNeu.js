window.onload = function () {
    $("document").ready(consulNeu);
}
function consulNeu() {
    let divConsulNeu = $("<div>");
    divConsulNeu.attr("id", "con");
    $("body").append(divConsulNeu);

    consulNeuJSON();
}
function consulNeuJSON() {
    $.ajax({
        'type': 'GET',
        'url': "./json/Neurologia/consultarDatosNeu.json",
        "async": true
        }
    ).done(conDatosNeu);
}
function conDatosNeu(objetoJSONConNeu) {

    arrayConNeu = objetoJSONConNeu.conNeu;
    arrayVolver = objetoJSONConNeu.volverConsulta;

    $.each(arrayConNeu, function (i, conNeu) {
        $.each(arrayVolver, function (i, volverConsulta) {

            //TITULO
            let elementH1 = $(conNeu.elementh1);
            elementH1.text(conNeu.titleh1);

            // FORMULARIO PARA EL BOT�N ACTUALIZAR DATOS
            let elementFORM = $(conNeu.elementForm);
            elementFORM.attr("action", conNeu.actionForm);
            elementFORM.attr("method", conNeu.methodForm);


            //BOT�N ACTUALIZAR DATOS
            let elementBUTTON = $(conNeu.elementButton);
            elementBUTTON.attr("type", conNeu.typeButton);
            elementBUTTON.attr("class", conNeu.classButton);
            elementBUTTON.text(conNeu.titleButton);

            //FORMULARIO PARA BOTON DE VOLVER A LA CONSULTA
            let elementFormVolver = $(conNeu.elementForm);
            elementFormVolver.attr("action", volverConsulta.actionFormVolver);
            elementFormVolver.attr("method", conNeu.methodForm);

            //BOT�N PARA VOLVER A LA CONSULTA
            let elementButtonVolver = $(conNeu.elementButton);
            elementButtonVolver.attr("type", conNeu.typeButton);
            elementButtonVolver.attr("class", conNeu.classButton);
            elementButtonVolver.text(volverConsulta.titleButton);

            // TABLA DE INSTRUCCIONES
            var tabla = document.createElement("TABLE");
            var filas = document.createElement("TR");
            var columna1 = document.createElement("TD");

            var tituloP1 = document.createElement("p");
            var titulo1 = document.createTextNode("ACTUALIZAR DATOS");
            var textoP1 = document.createElement("p");
            var texto1 = document.createTextNode("Si los datos no son correctos, pulsar el bot�n de actualizer datos");

            var tituloP2 = document.createElement("p");
            var titulo2 = document.createTextNode("VOLVER");
            var textoP2 = document.createElement("p");
            var texto2 = document.createTextNode("Si los datos son correctos, pulsar el boton de volver");

            columna1.style.width="870px";
            tituloP1.className = "encabezadoAmarillo";
            textoP1.className = "cuerpoBlanco";
            tituloP2.className = "encabezadoAmarillo";
            textoP2.className = "cuerpoBlanco";

            var columna2 = document.createElement("TD");

            var img = document.createElement("IMG");
            img.setAttribute("src", "img/ImagenDatos.png");

            columna2.style.width="960px";
            img.className = "imgInst";

            // IMPRIMIR TITULO POR PANTALLA
            $("#con").append(elementH1);

            //IMPRIMIR FORMULARIO POR PANTALLA
            $("#con").append(elementFORM);

            //IMPRIMIR BOT�N ACTUALIZAR DATOS POR PANTALLA
            $(elementFORM).append(elementBUTTON);

            //IMPRIMIR BOTON VOLVER A CONSULTA
            $("#con").append(elementFormVolver);
            $(elementFormVolver).append(elementButtonVolver);

            //IMPRIMIR TABLA DE INSTRUCCIONES
            document.body.appendChild(tabla);
            tabla.appendChild(filas);
            filas.appendChild(columna1);
            columna1.appendChild(tituloP1);
            tituloP1.appendChild(titulo1);
            columna1.appendChild(textoP1);
            textoP1.appendChild(texto1);
            columna1.appendChild(tituloP2);
            tituloP2.appendChild(titulo2);
            columna1.appendChild(textoP2);
            textoP2.appendChild(texto2);
            filas.appendChild(columna2);
            columna2.appendChild(img);

        })
    })

}