window.onload = function (){
    $(document).ready(avisarPaciente);
}
function avisarPaciente(){
    let divAvisar = $("<div>");
    divAvisar.attr("id", "avi");
    $("body").append(divAvisar);

    avisarJSON();
}
function avisarJSON(){
    $.ajax({
        'type' : 'GET',
        'url' : "./json/Neurologia/avisarPacienteNeu.json",
        "async": true
        }
    ).done(insertarDatosAvisar)
}
function insertarDatosAvisar(objetoJSONAvisar){

    arrayInsertarAvisar = objetoJSONAvisar.avisarPacNeu;
    arrayNombre = objetoJSONAvisar.datosNombreNeu;
    arrayAp1 = objetoJSONAvisar.datosApellido1Neu;
    arrayAp2 = objetoJSONAvisar.datosApellido2Neu;
    arrayDni = objetoJSONAvisar.datosDNINeu;

    $.each(arrayInsertarAvisar, function (i, avisarPacNeu){
    $.each(arrayNombre, function (i, datosNombreNeu){
    $.each(arrayAp1, function (i, datosApellido1Neu){
        $.each(arrayAp2, function (i, datosApellido2Neu){
            $.each(arrayDni, function (i, datosDNINeu){

                //TITULO
                let elementH1 = $(avisarPacNeu.elementh1);
                elementH1.text(avisarPacNeu.titleh1);

                //FORMULARIO
                let elementForm = $(avisarPacNeu.elementFORM);
                elementForm.attr("action", avisarPacNeu.actionFORM);
                elementForm.attr("method", avisarPacNeu.methodFORM);

                //CAMPO NOMBRE
                let elementDivNombre = $(avisarPacNeu.elementDIV);
                elementDivNombre.attr("class", avisarPacNeu.classDIV);

                let elementLabelNombre = $(avisarPacNeu.elementLABEL);
                elementLabelNombre.attr("for", datosNombreNeu.forNombre);
                elementLabelNombre.text(datosNombreNeu.titleNombre);

                let elementInputNombre = $(avisarPacNeu.elementINPUT);
                elementInputNombre.attr("type", avisarPacNeu.tyoeINPUT);
                elementInputNombre.attr("name", datosNombreNeu.nameNombre);
                elementInputNombre.attr("class", avisarPacNeu.classINPUT);
                elementInputNombre.attr("id", datosNombreNeu.idNombre);

                //CAMPO PRIMER APELLIDO
                let elementDivAp1 = $(avisarPacNeu.elementDIV);
                elementDivAp1.attr("class", avisarPacNeu.classDIV);

                let elementLabelAp1 = $(avisarPacNeu.elementLABEL);
                elementLabelAp1.attr("for", datosApellido1Neu.forApellido1);
                elementLabelAp1.text(datosApellido1Neu.titleApellido1);

                let elementInputAp1 = $(avisarPacNeu.elementINPUT);
                elementInputAp1.attr("type", avisarPacNeu.tyoeINPUT);
                elementInputAp1.attr("name", datosApellido1Neu.nameApellido1);
                elementInputAp1.attr("class", avisarPacNeu.classINPUT);
                elementInputAp1.attr("id", datosApellido1Neu.idApellido1);

                //CAMPO SEGUNDO APELLIDO
                let elementDivAp2 = $(avisarPacNeu.elementDIV);
                elementDivAp2.attr("class", avisarPacNeu.classDIV);

                let elementLabelAp2 = $(avisarPacNeu.elementLABEL);
                elementLabelAp2.attr("for", datosApellido2Neu.forApellido2);
                elementLabelAp2.text(datosApellido2Neu.titleApellido2);

                let elementInputAp2 = $(avisarPacNeu.elementINPUT);
                elementInputAp2.attr("type", avisarPacNeu.tyoeINPUT);
                elementInputAp2.attr("name", datosApellido2Neu.nameApellido2);
                elementInputAp2.attr("class", avisarPacNeu.classINPUT);
                elementInputAp2.attr("id", datosApellido2Neu.idApellido2);



                //CAMPO DNI
                let elementDivDni = $(avisarPacNeu.elementDIV);
                elementDivDni.attr("class", avisarPacNeu.classDIV);

                let elementLabelDni= $(avisarPacNeu.elementLABEL);
                elementLabelDni.attr("for", datosDNINeu.forDNI);
                elementLabelDni.text(datosDNINeu.titleDNI);

                let elementInputDni = $(avisarPacNeu.elementINPUT);
                elementInputDni.attr("type", avisarPacNeu.tyoeINPUTN);
                elementInputDni.attr("name", datosDNINeu.nameDNI);
                elementInputDni.attr("class", avisarPacNeu.classINPUT);
                elementInputDni.attr("id", datosDNINeu.idDNI);

                //BOTON DE AVISAR AL PACIENTE
                let elementButton = $(avisarPacNeu.elementBUTTON);
                elementButton.attr("type", avisarPacNeu.typeBUTTON);
                elementButton.attr("class", avisarPacNeu.classBUTTON);
                elementButton.text(avisarPacNeu.titleBUTTON);

                // TABLA DE INSTRUCCIONES
                var tabla = document.createElement("TABLE");
                var filas = document.createElement("TR");
                var columna1 = document.createElement("TD");

                var tituloP1 = document.createElement("p");
                var titulo1 = document.createTextNode("AVISAR AL PACIENTE");
                var textoP1 = document.createElement("p");
                var texto1 = document.createTextNode("Rellenar el formulario y pulsar el boton de avisar al paciente.\n");

                columna1.style.width="960px";
                tituloP1.className = "encabezadoAmarillo";
                textoP1.className = "cuerpoBlanco";

                var columna2 = document.createElement("TD");

                var img = document.createElement("IMG");
                    img.setAttribute("src", "img/ImagenLlamarPaciente.png");

                columna2.style.width="960px";
                img.className = "imgInst";

                //IMPRIMIR TITULO POR PANTALLA
                $("#avi").append(elementH1);

                //IMPRIMIR FORMULARIO POR PANTALLA
                $("#avi").append(elementForm);

                //IMPRIMIR CAMPO NOMBRE POR PANTALLA
                $(elementForm).append(elementDivNombre);
                $(elementDivNombre).append(elementLabelNombre);
                $(elementDivNombre).append(elementInputNombre);

                //IMPRIMIR CAMPO PRIMER APELLIDO POR PANTALLA
                $(elementForm).append(elementDivAp1);
                $(elementDivAp1).append(elementLabelAp1);
                $(elementDivAp1).append(elementInputAp1);

                //IMPRIMIR CAMPO SEGUNDO APELLIDO POR PANTALLA
                $(elementForm).append(elementDivAp2);
                $(elementDivAp2).append(elementLabelAp2);
                $(elementDivAp2).append(elementInputAp2);

                //IMPRIMIR CAMPO DNI POR PANTALLA
                $(elementForm).append(elementDivDni);
                $(elementDivDni).append(elementLabelDni);
                $(elementDivDni).append(elementInputDni);

                //IMPRIMIR BOT�N AVISAR AL PACIENTE
                $(elementForm).append(elementButton);

                //IMPRIMIR TABLA DE INSTRUCCIONES
                document.body.appendChild(tabla);
                tabla.appendChild(filas);
                filas.appendChild(columna1);
                columna1.appendChild(tituloP1);
                tituloP1.appendChild(titulo1);
                columna1.appendChild(textoP1);
                textoP1.appendChild(texto1);
                filas.appendChild(columna2);
                columna2.appendChild(img);

            })
        })
    })
    })
    })
}