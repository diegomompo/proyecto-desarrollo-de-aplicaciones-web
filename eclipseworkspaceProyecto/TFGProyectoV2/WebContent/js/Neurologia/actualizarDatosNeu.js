window.onload = function () {
    $("document").ready(actNeu);
}
function actNeu() {
    let divActualizarNeu = $("<div>");
    divActualizarNeu.attr("id", "act");
    $("body").append(divActualizarNeu);

    actualizarNeuJSON();
}
function actualizarNeuJSON() {
    $.ajax({
        'type' : 'GET',
        'url' : "./json/Neurologia/actualizarDatosNeu.json",
        "async": true
    }
    ).done(actDatosNeu);
}
function actDatosNeu(objetoJSONActNeu) {

    arrayActNeu = objetoJSONActNeu.actuaNeu;
    arrayPesoNeu = objetoJSONActNeu.datosPesoNeu;
    arrayAturaNeu = objetoJSONActNeu.datosAlturaNeu;

    $.each(arrayActNeu, function (i, actuaNeu) {
    $.each(arrayPesoNeu, function (i, datosPesoNeu) {
    $.each(arrayAturaNeu, function (i, datosAlturaNeu) {

        //TITULO
        let elementH1 = $(actuaNeu.elementh1);
        elementH1.text(actuaNeu.titleh1);

        //FORMULARIO PARA MODIFICAR LOS DATOS
        let elementFORM = $(actuaNeu.elementForm);
        elementFORM.attr("action", actuaNeu.actionForm);
        elementFORM.attr("method", actuaNeu.methodForm);

        //CAMPO PESO
        let elementDivPeso = $(actuaNeu.elementDiv);
        elementDivPeso.attr("class", actuaNeu.classDiv);

        let elementLabelPeso = $(actuaNeu.elementLabel);
        elementLabelPeso.attr("for", datosPesoNeu.forPeso);
        elementLabelPeso.text(datosPesoNeu.titlePeso);

        let elementInputPeso = $(actuaNeu.elementInput);
        elementInputPeso.attr("type", actuaNeu.typeInputD);
        elementInputPeso.attr("name", datosPesoNeu.namePeso);
        elementInputPeso.attr("class", actuaNeu.classInput);
        elementInputPeso.attr("id", datosPesoNeu.idPeso);

        //CAMPO ALTURA
        let elementDivAltura = $(actuaNeu.elementDiv);
        elementDivAltura.attr("class", actuaNeu.classDiv);

        let elementLabelAltura = $(actuaNeu.elementLabel);
        elementLabelAltura.attr("for", datosAlturaNeu.forAltura);
        elementLabelAltura.text(datosAlturaNeu.titleAltura);

        let elementInputAltura = $(actuaNeu.elementInput);
        elementInputAltura.attr("type", actuaNeu.typeInputD);
        elementInputAltura.attr("name", datosAlturaNeu.nameAltura);
        elementInputAltura.attr("class", actuaNeu.classInput);
        elementInputAltura.attr("id", datosAlturaNeu.idAltura);

        // BOTON MODIFICAR DATOS
        let elementBUTTON = $(actuaNeu.elementButton);
        elementBUTTON.attr("type", actuaNeu.typeButton);
        elementBUTTON.attr("class", actuaNeu.classButton);
        elementBUTTON.text(actuaNeu.titleButton);

        // TABLA DE INSTRUCCIONES
        var tabla = document.createElement("TABLE");
        var filas = document.createElement("TR");
        var columna1 = document.createElement("TD");

        var tituloP1 = document.createElement("p");
        var titulo1 = document.createTextNode("MODIFICAR DATOS");
        var textoP1 = document.createElement("p");
        var texto1 = document.createTextNode("Rellenar los nuevos datos de PESO y ALTURA");

        columna1.style.width="870px";
        tituloP1.className = "encabezadoAmarillo";
        textoP1.className = "cuerpoBlanco";

        var columna2 = document.createElement("TD");

        var img = document.createElement("IMG");
        img.setAttribute("src", "img/ImagenDatos.png");

        columna2.style.width="960px";
        img.className = "imgInst";


        //IMPRIMIR TITULO POR PANTALLA
        $("#act").append(elementH1);

        //IMPRIMIR FORMULARIO POR PANTALLA
        $("#act").append(elementFORM);

        // IMPRIMIR CAMPO PESO POR PANTALLA
        $(elementFORM).append(elementDivPeso);
        $(elementDivPeso).append(elementLabelPeso);
        $(elementDivPeso).append(elementInputPeso);

        // IMPRIMIR CAMPO ALTURA POR PANTALLA
        $(elementFORM).append(elementDivAltura);
        $(elementDivAltura).append(elementLabelAltura);
        $(elementDivAltura).append(elementInputAltura);

        //IMPRIMIR BUTTON
        $(elementFORM).append(elementBUTTON);

        //IMPRIMIR TABLA DE INSTRUCCIONES
        document.body.appendChild(tabla);
        tabla.appendChild(filas);
        filas.appendChild(columna1);
        columna1.appendChild(tituloP1);
        tituloP1.appendChild(titulo1);
        columna1.appendChild(textoP1);
        textoP1.appendChild(texto1);
        filas.appendChild(columna2);
        columna2.appendChild(img);
    })
    })
    })
}