function move() {
    var elem = document.getElementById("barra"); //
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= 100) {
            clearInterval(id); //Se para de mover la barra si el intervalo es mayor o igual que 100
        } else {
            width++;
            elem.style.width = width + '%';
            $("#porcentaje").text(width * 1 + '%'); //Imprime el porcentaje
        }
    }
}