window.onload = function(){
	$(document).ready(consultaTurno);
}
function consultaTurno(){
	let divEspera = $("<div>");
	divEspera.attr("id", "esp");
	$("body").append(divEspera);
	
	EsperaJSON();
}
function EsperaJSON(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/espera.json",
		"async": true
		}
	).done(datosEspera)
}
function datosEspera(objetoJSONDatos){
	
	arrayInsertar = objetoJSONDatos.registrarDatos;
	arrayNombre = objetoJSONDatos.datosNombre;
	arrayApellido1 = objetoJSONDatos.datosApellido1;
	arrayApellido2 = objetoJSONDatos.datosApellido2;
	arrayVolverInicio = objetoJSONDatos.volverInicio;
	arrayDNI = objetoJSONDatos.datosDNI;
	
	$.each(arrayInsertar, function(i, registrarDatos){
	$.each(arrayNombre, function(i, datosNombre){
	$.each(arrayApellido1, function(i, datosApellido1){
	$.each(arrayApellido2, function(i, datosApellido2){
	$.each(arrayVolverInicio, function(i, volverInicio){
		$.each(arrayDNI, function(i, datosDNI){

			//TITULO
			let elementh1 = $(registrarDatos.elementH1);
			elementh1.text(registrarDatos.title1);

			//FORMULARIO
			let elementForm = $(registrarDatos.elementFORM);
			elementForm.attr("action", registrarDatos.actionFORM);
			elementForm.attr("method", registrarDatos.methodFORM);

			//INSERTAR EL NOMBRE
			let elementDivNombre = $(registrarDatos.elementDIV);
			elementDivNombre.attr("class", registrarDatos.classDIV);

			let elementLabelNombre = $(registrarDatos.elementLABEL);
			elementLabelNombre.attr("for", datosNombre.forNombre);
			elementLabelNombre.text(datosNombre.titleNombre);

			let elementInputNombre = $(registrarDatos.elementINPUT);
			elementInputNombre.attr("type", registrarDatos.typeINPUT);
			elementInputNombre.attr("name", datosNombre.nameNombre);
			elementInputNombre.attr("class", registrarDatos.classINPUT);
			elementInputNombre.attr("id", datosNombre.idNombre);

			//INSERTAR EL PRIMER APELLIDO
			let elementDivAp1 = $(registrarDatos.elementDIV);
			elementDivAp1.attr("class", registrarDatos.classDIV);

			let elementLabelAp1 = $(registrarDatos.elementLABEL);
			elementLabelAp1.attr("for", datosApellido1.forApellido1);
			elementLabelAp1.text(datosApellido1.titleApellido1);

			let elementInputAp1 = $(registrarDatos.elementINPUT);
			elementInputAp1.attr("type", registrarDatos.typeINPUT);
			elementInputAp1.attr("name", datosApellido1.nameApellido1);
			elementInputAp1.attr("class", registrarDatos.classINPUT);
			elementInputAp1.attr("id", datosApellido1.idApellido1);

			//INSERTAR EL SEGUNDO APELLIDO
			let elementDivAp2 = $(registrarDatos.elementDIV);
			elementDivAp2.attr("class", registrarDatos.classDIV);

			let elementLabelAp2 = $(registrarDatos.elementLABEL);
			elementLabelAp2.attr("for", datosApellido2.forApellido2);
			elementLabelAp2.text(datosApellido2.titleApellido2);

			let elementInputAp2 = $(registrarDatos.elementINPUT);
			elementInputAp2.attr("type", registrarDatos.typeINPUT);
			elementInputAp2.attr("name", datosApellido2.nameApellido2);
			elementInputAp2.attr("class", registrarDatos.classINPUT);
			elementInputAp2.attr("id", datosApellido2.idApellido2);

			//CAMPO DNI
			let elementDivDni = $(registrarDatos.elementDIV);
			elementDivDni.attr("class", registrarDatos.classDIV);

			let elementLabelDni= $(registrarDatos.elementLABEL);
			elementLabelDni.attr("for", datosDNI.forDNI);
			elementLabelDni.text(datosDNI.titleDNI);

			let elementInputDni = $(registrarDatos.elementINPUT);
			elementInputDni.attr("type", registrarDatos.typeINPUT2);
			elementInputDni.attr("name", datosDNI.nameDNI);
			elementInputDni.attr("class", registrarDatos.classINPUT);
			elementInputDni.attr("id", datosDNI.idDNI);

			//BOTON DE REGISTRAR
			let elementButton = $(registrarDatos.elementBUTTON);
			elementButton.attr("type", registrarDatos.typeButton);
			elementButton.attr("class", registrarDatos.classButton);
			elementButton.text(registrarDatos.titleButton);

			//ACCIÓN PARA VOLVER
			let elementFVolver = $(volverInicio.elementFormVolver);
			elementFVolver.attr("action", volverInicio.actionFormVolver);
			elementFVolver.attr("method", volverInicio.methodFormVolver);

			//BOTÓN VOLVER

			let elementBVolver = $(volverInicio.elementButtonVolver);
			elementBVolver.attr("type", volverInicio.typeButtonVolver);
			elementBVolver.attr("class", volverInicio.classButtonVolver);
			elementBVolver.text(volverInicio.titleButtonVolver);

			// TABLA DE INSTRUCCIONES
			var tabla = document.createElement("TABLE");
			var filas = document.createElement("TR");
			var columna1 = document.createElement("TD");

			var tituloP1 = document.createElement("p");
			var titulo1 = document.createTextNode("ENTRAR A LA SALA DE ESPERA");
			var textoP1 = document.createElement("p");
			var texto1 = document.createTextNode("Identificar al paciente rellenando los datos del forumulario y pulsar el boton entrar en la sala de espera.");
			var textoP2 = document.createElement("p");
			var texto2 = document.createTextNode("El paciente se sienta en la sala de espera a esperar a que lo llamen.");

			var tituloP2 = document.createElement("p");
			var titulo2 = document.createTextNode("VOLVER AL INICIO");
			var textoP3 = document.createElement("p");
			var texto3 = document.createTextNode("Si el paciente no tiene turno saldrá un mensaje de error y hay que volver a la pantalla de inicio para dar turno.\n");

			columna1.style.width="960px";
			tituloP1.className = "encabezadoAmarillo";
			textoP1.className = "cuerpoBlanco"
			textoP2.className = "cuerpoBlanco";
			tituloP2.className = "encabezadoAmarillo";
			textoP3.className = "cuerpoBlanco";

			var columna2 = document.createElement("TD");

			var img = document.createElement("IMG");
			img.setAttribute("src", "img/ImagenComTurno.png");

			columna2.style.width="960px";
			img.className = "imgInst";

			//IMPRIMIR TITULO POR PANTALLA
			$("#esp").append(elementh1);

			//IMPRIMIR FORMUALRIO POR PANTALLA
			$("#esp").append(elementForm);

			//IMPRIMIR CAMPO NOMBRE POR PANTALLA
			$(elementForm).append(elementDivNombre);
			$(elementDivNombre).append(elementLabelNombre);
			$(elementDivNombre).append(elementInputNombre);

			//IMPRIMIR CAMPO PRIMER APELLIDO POR PANTALLA
			$(elementForm).append(elementDivAp1);
			$(elementDivAp1).append(elementLabelAp1);
			$(elementDivAp1).append(elementInputAp1);

			//IMPRIMIR CAMPO SEGUNDO APELLIDO POR PANTALLA
			$(elementForm).append(elementDivAp2);
			$(elementDivAp2).append(elementLabelAp2);
			$(elementDivAp2).append(elementInputAp2);

			//IMPRIMIR CAMPO DNI POR PANTALLA
			$(elementForm).append(elementDivDni);
			$(elementDivDni).append(elementLabelDni);
			$(elementDivDni).append(elementInputDni);

			//IMPRIMIR BOTÓN REGISTRAR
			$(elementForm).append(elementButton);

			//IMPRIMIR ACCIÓN VOLVER POR PANTALLA
			$("#esp").append(elementFVolver);

			//IMPRIMIR BOTÓN VOLVER
			$(elementFVolver).append(elementBVolver);

			//IMPRIMIR TABLA DE INSTRUCCIONES
			document.body.appendChild(tabla);
			tabla.appendChild(filas);
			filas.appendChild(columna1);
			columna1.appendChild(tituloP1);
			tituloP1.appendChild(titulo1);
			columna1.appendChild(textoP1);
			textoP1.appendChild(texto1);
			columna1.appendChild(textoP2);
			textoP2.appendChild(texto2);
			columna1.appendChild(tituloP2);
			tituloP2.appendChild(titulo2);
			columna1.appendChild(textoP3);
			textoP3.appendChild(texto3);
			filas.appendChild(columna2);
			columna2.appendChild(img);
		})
	})
	})
	})
	})
	})
	
}

/**
 * 
 */