//BARRA de PREPARAR EL FORMULARIO PARA EL TURNO

function barraPreparandoFormMedico(){

    var header1 = $("<h1>");
    header1.text("Preparando del formulario para seleccionar el medico");
    $("body").append(header1);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main?op=solicitandoTurno");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();

    move();
}

//-------------------------------------------------------------------------------------------------
//BARRA de PREPARAR EL FORMULARIO PARA EL TURNO

function barraPreparandoFormTurno(){

    var header1 = $("<h1>");
    header1.text("Preparando del formulario para consultar el turno");
    $("body").append(header1);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra?op=yendoSolicitandoTurno");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();

    move();
}

//-------------------------------------------------------------------------------------------------

// BARRA de INSERTARLOS DATOS DEL TURNO

function barraInsertando(){

    var header3 = $("<h1>");
    header3.text("Insertando los datos del turno en la base de datos");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main_Barra?op=barraInsertandoDatos");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();

    move();
}
//-------------------------------------------------------------------------------------------------

// BARRA de VOLVER

function barraVolviendo(){

    var header3 = $("<h1>");
    header3.text("Volviendo al inicio");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main?op=volviendoInicio");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();

    move();
}
//-------------------------------------------------------------------------------------------------

// BARRA de CONSULTAR DATOS PARA SALA DE ESPERA

function barraPreparandoFormEspera() {

    var header3 = $("<h1>");
    header3.text("Preparando el formulario para los datos de sala de espera");
    $("body").append(header3);
    cuerpoBarra()

    var elementForm = $("<form>");
    elementForm.attr("action", "Main?op=irSala");
    elementForm.attr("method", "post");
    elementForm.attr("id", "ir");

    $("body").append(elementForm);

    cerrarButton();
    move();
}

//-------------------------------------------------------------------------------------------------

// FUNCION QUE DEFINE EL CUERPO DE LA BARRA Y EK BOTON DE CERRAR LA BARRA

    function cuerpoBarra() {
        var fondo = $("<div>");
        fondo.attr("class", "estiloFondo");
        fondo.attr("id", "eFondo");

        var eBarra = $("<div>");
        eBarra.attr("class", "estiloBarra");
        eBarra.attr("id", "barra");

        var ePorcentaje = $("<div>");
        ePorcentaje.attr("class", "estiloPorcentaje");
        ePorcentaje.attr("id", "porcentaje");
        ePorcentaje.text("0%");

        $("body").append(fondo);
        $("#eFondo").append(eBarra);
        $("#barra").append(ePorcentaje);
    }

//--------------------------------------------------------------------------------------------------------

//FUNCION PARA EL CERRAR EL BOTON

    function cerrarButton() {
        var botonCerrar = $("<button>");

        botonCerrar.attr("type", "submit");
        botonCerrar.attr("class", "btn btn-warning");
        botonCerrar.text("Cerrar");

        $("#ir").append(botonCerrar);
    }