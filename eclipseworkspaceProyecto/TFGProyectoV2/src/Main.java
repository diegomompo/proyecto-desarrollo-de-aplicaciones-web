

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.pacienteNeurologia;
import beans.Neurologia.turnoNeurologia;
import beans.TipoMedico;
import beans.especialidades;
import turno.BorrarTurnoDaoImpl;
import turno.ConsultaTurnoDaoImpl;
import turno.InsertarDatosTurnoDaoImpl;


/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// SELECCIONAR OPCION

		String op = request.getParameter("op");


		switch(op) {

			case "solicitandoTurno":
				pedirTurno(request, response);
				break;
			case "seleccionandoMedico":
				seleccionarMedico(request, response);
				break;
			case "insertandoDatos":
				insertarDatos(request, response);
				break;
			case "irSala":
				yendoSalaEspera(request, response);
			break;
			case "consultandoTurno":
				consultarTurno(request, response);
			break;
			case "volviendoInicio":
				volverInicio(request, response);
			break;
			case "terminandoConsulta":
				terminarConsulta(request, response);
			break;
		}
	}


	// IR A LA P�GINA TURNO
	private void pedirTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// TODO Auto-generated method stub


		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();

		TipoMedico espMed = ctdao.findbyTMed(1);
		session.setAttribute("espMed", espMed);
		TipoMedico espMedV = (TipoMedico) session.getAttribute("espMed");

		List<medicoNeurologia> medicos;
		medicos = ctdao.medico(espMedV);

		System.out.println(medicos);

		String sMedico[] = new String[medicos.size()];


		for(int i=0; i<medicos.size(); i++) {
			medicos.get(i).getEspecialidades().getEspecialidad();
			sMedico[i] = medicos.get(i).getEspecialidades().getEspecialidad() + " - " + medicos.get(i).getNombre() + " " + medicos.get(i).getApellido1() + " " + medicos.get(i).getApellido2();
		}

		session.setAttribute("sMedico", sMedico);
		request.getRequestDispatcher("Medico.jsp").forward(request, response);
	}

	private void seleccionarMedico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String selMedico = request.getParameter("medico");

		int idMedico = Integer.parseInt(selMedico);

		session.setAttribute("idMedico", idMedico);
		session.setAttribute("selMedico", selMedico);

		request.getRequestDispatcher("barra_preparandoTurno.html").forward(request, response);

	}
	// REGISTRANDO EL TURNO
	private void insertarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;

		String nom = request.getParameter("nombre");
		String ap1 = request.getParameter("apellido1");
		String ap2 = request.getParameter("apellido2");
		int dni = Integer.parseInt(request.getParameter("dni"));
		int idMed = (int) session.getAttribute("idMedico");


		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		InsertarDatosTurnoDaoImpl idtdao = new InsertarDatosTurnoDaoImpl();


				List<turnoNeurologia> tExiste = ctdao.findbyNombreAp(nom, ap1, dni);


				if (tExiste.size() == 0) {

					turnoNeurologia tur = new turnoNeurologia(0, nom, ap1, ap2, dni, idMed);
					idtdao.insertTurn(tur);

					request.getRequestDispatcher("barra_insertandoTurno.html").forward(request, response);
				}else {
					msj = "El paciente ya tiene un turno";
					request.setAttribute("msj", msj);
					request.getRequestDispatcher("Turno.jsp").forward(request, response);
				}
	}
	
	//IR A LA SALA DE ESPERA

	private void yendoSalaEspera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
			request.getRequestDispatcher("ConsultaEspera.jsp").forward(request, response);
	}
	
	private void consultarTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		String nom = request.getParameter("nombre");
		String ap1 = request.getParameter("apellido1");
		String ap2 = request.getParameter("apellido2");
		int dni = Integer.parseInt(request.getParameter("dni"));
		
		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		InsertarDatosTurnoDaoImpl idtdao = new InsertarDatosTurnoDaoImpl();

		List<turnoNeurologia> tExiste = ctdao.findbyNombreAp(nom, ap1, dni);

					if (tExiste.size() != 0) {

						System.out.println("Est� dentro");
						request.getRequestDispatcher("Neurologia/barras/barra_yendoSalaEspera.html").forward(request, response);
					}
					else {
						msj = "El paciente introducirdo no tiene Turno. Por favor introduzca uno";
						request.setAttribute("msj", msj);
						request.getRequestDispatcher("ConsultaEspera.jsp").forward(request, response);
					}
	}
	
	private void volverInicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.getRequestDispatcher("Inicio.jsp").forward(request, response);
	}

	// TERMIMAR LA CONSULTA
	private void terminarConsulta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();

		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		BorrarTurnoDaoImpl btdao = new BorrarTurnoDaoImpl();

		int dni = (Integer)session.getAttribute("dni");

		List<turnoNeurologia> cDNI =  ctdao.conDNIPac(dni);

		int idTurn = cDNI.get(0).getIdPacienteTurno();

		turnoNeurologia turnI = btdao.findbyNum(idTurn);


		System.out.println("filas: " + btdao.deleteTurno(turnI));

 		request.getRequestDispatcher("Neurologia/SalaEsperaNeu.jsp").forward(request, response);
	}

}
