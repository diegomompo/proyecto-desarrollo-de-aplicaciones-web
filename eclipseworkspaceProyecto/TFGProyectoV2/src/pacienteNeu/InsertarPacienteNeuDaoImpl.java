package pacienteNeu;

import beans.Neurologia.pacienteNeurologia;
import dao.AbstractClinicaDao;

public class InsertarPacienteNeuDaoImpl extends AbstractClinicaDao implements IntInsertarPacienteNeuDaoImpl{
    @Override
    public int insDatos(pacienteNeurologia pacNeu) {
        int filasInsertarPacienteNeu = 0;

        try{
            tx.begin();
            em.persist(pacNeu);
            tx.commit();
            filasInsertarPacienteNeu = 1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return filasInsertarPacienteNeu;
    }
}
