package pacienteNeu;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.pacienteNeurologia;
import dao.AbstractClinicaDao;

import java.util.List;

public class ConsultaPacienteNeuDaoImpl extends AbstractClinicaDao implements IntConsultarPacienteNeuDaoImpl {


	@Override
	public List<pacienteNeurologia> consultarTurno(String nombre, String apellido, int dni) {
		sql = "SELECT p FROM pacienteNeurologia p WHERE p.nombre = :nom AND p.apellido1 = :ap1 AND p.dni = :DNI";
		query = em.createQuery(sql);
		query.setParameter("nom", nombre);
		query.setParameter("ap1", apellido);
		query.setParameter("DNI", dni);
		return query.getResultList();
	}

	@Override
	public List<pacienteNeurologia> conDNIPac(int dni) {
		sql = "SELECT p FROM pacienteNeurologia p WHERE  p.dni = :DNI";
		query = em.createQuery(sql);
		query.setParameter("DNI", dni);
		return query.getResultList();
	}
	@Override
	public pacienteNeurologia findIdPac(int idPac) {
		return em.find(pacienteNeurologia.class, idPac);
	}
	public medicoNeurologia findbyMed(int idMed) {
		return em.find(medicoNeurologia.class, idMed);
	}
}
