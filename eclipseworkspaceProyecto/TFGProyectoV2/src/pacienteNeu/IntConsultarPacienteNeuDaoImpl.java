package pacienteNeu;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.pacienteNeurologia;

import java.util.List;

public interface IntConsultarPacienteNeuDaoImpl {
	List<pacienteNeurologia> consultarTurno(String nombre, String apellido, int dni);
	List<pacienteNeurologia> conDNIPac(int dni);
	pacienteNeurologia findIdPac(int idPac);
	medicoNeurologia findbyMed(int idMed);
}
