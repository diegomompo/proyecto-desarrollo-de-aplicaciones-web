package pacienteNeu;

import beans.Neurologia.pacienteNeurologia;
import dao.AbstractClinicaDao;

public class ModificarPacienteNeuDaoImpl extends AbstractClinicaDao implements IntModificarPacienteNeuDaoImpl {


	@Override
	public int modDatos(pacienteNeurologia pacNeu) {
		int filasPacNeu = 0;

		try{
			tx.begin();
			em.merge(pacNeu);
			tx.commit();
			filasPacNeu = 1;
		}catch (Exception e){
			e.printStackTrace();
		}
		return filasPacNeu;
	}
}
