import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;


/**
 * Servlet implementation class Main
 */
@WebServlet("/Main_Barra")
public class Main_Barra extends HttpServlet {
	private static final SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Main_Barra() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// SELECCIONAR OPCION

		String op = request.getParameter("op");


		switch (op) {

			case "yendoSeleccionandoMedico":
				barraSeleccionarMedico(request, response);
				break;
			case "yendoSolicitandoTurno":
				barraPedirTurno(request, response);
				break;
			case "barraInsertandoDatos":
				barraInsertarDatos(request, response);
				break;
			case "barraVolviendoInicio":
				barraVolverInicio(request, response);
				break;
			case "barraIrConsultarTurno":
				barraConsultaTurno(request, response);
				break;

		}
	}

	private void barraSeleccionarMedico(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("barra_preparandoMedico.html").forward(request, response);
	}


	private void barraPedirTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Turno.jsp").forward(request, response);
	}

	private void barraInsertarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Inicio.jsp").forward(request, response);
	}

	private void barraVolverInicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("barra_volviendoInicio.html").forward(request, response);

	}
	private void barraConsultaTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("barra_preparandoConsultaEspera.html").forward(request, response);
	}

}
	
