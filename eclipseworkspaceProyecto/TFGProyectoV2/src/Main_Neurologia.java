import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.turnoNeurologia;
import pacienteNeu.ConsultaPacienteNeuDaoImpl;
import pacienteNeu.InsertarPacienteNeuDaoImpl;
import pacienteNeu.ModificarPacienteNeuDaoImpl;
import turno.ConsultaTurnoDaoImpl;
import beans.Neurologia.pacienteNeurologia;


/**
 * Servlet implementation class Main
 */
@WebServlet("/Main_Neurologia")
public class Main_Neurologia extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main_Neurologia() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
     */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String op = request.getParameter("op");
        switch(op) {
            case "llamandoPacienteNeu":
                llamarPacienteNeu(request, response);
                break;
            case "avisandoPacienteNeu":
                avisarPacienteNeu(request, response);
                break;
            case "entrandoConsultaNeu":
                entrarConsultaNeu(request, response);
                break;
            case "yendoInsertarDatosPacienteNeu":
                irInsertarDatosNeu(request, response);
                break;
            case "insertandoDatosPacienteNeu":
                insertarDatosPacienteNeu(request, response);
                break;
            case "consultandoDatosPaciente":
                consultarDatosNeu(request, response);
                break;
            case "volviendoConsulta":
                volverConsulta(request, response);
                break;
            case "yendoModificarDatosPacienteNeu":
                irModidicarDatos(request, response);
                break;
            case "modificandoDatosPacienteNeu":
                modificarDatos(request, response);
                break;
        }
    }

    // LLAMAR A UN PACIENTE

    private void llamarPacienteNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Neurologia/AvisarPacienteNeu.jsp").forward(request, response);
    }
    private void avisarPacienteNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String nom = request.getParameter("nombre");
        String ap1 = request.getParameter("apellido1");
        String ap2 = request.getParameter("apellido2");
        int dni = Integer.parseInt(request.getParameter("dni"));

        ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();

        List<turnoNeurologia> turn = new ArrayList<turnoNeurologia>();

        turn = ctdao.findbyNombreAp(nom, ap1, dni);

        int id_medico = turn.get(0).getIdMedico();

        System.out.println(turn);

        session.setAttribute("avisaNombre", nom);
        session.setAttribute("avisaApellido", ap1);
        session.setAttribute("avisaApellido2", ap2);
        session.setAttribute("dni", dni);
        session.setAttribute("id_medico", id_medico);
        session.setAttribute("turn", turn);

        request.getRequestDispatcher("Neurologia/barras/barra_llamandoPaciente.html").forward(request, response);
    }

    // ENTRAR EN LA CONSULTA
    private void entrarConsultaNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        int idMed =(int) session.getAttribute("id_medico");

        ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();

        List<medicoNeurologia> med = new ArrayList<medicoNeurologia>();
        med = ctdao.findByMed(idMed);

        String iMed[] = new String[med.size()];
        iMed[0] = med.get(0).getNombre() + " " +  med.get(0).getApellido1() + " " + med.get(0).getApellido2();

        session.setAttribute("iMed", iMed);

        request.getRequestDispatcher("Neurologia/ConsultaNeu.jsp").forward(request, response);
    }

    // IR A FORMULARIO PARA INSERTAR DATOS
    private void irInsertarDatosNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Neurologia/InsertarDatosNeu.jsp").forward(request,response);
    }

    //INSERTAR EL DATOS DEL NUEVO PACIENTE
    private void insertarDatosPacienteNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String nom = (String) session.getAttribute("avisaNombre");
        String ap1 = (String) session.getAttribute("avisaApellido");
        String ap2 = (String) session.getAttribute("avisaApellido2");
        int dni = (Integer)session.getAttribute("dni");
        int idMed = (Integer)session.getAttribute("id_medico");
        double peso = Double.parseDouble(request.getParameter("peso"));
        int altura = Integer.parseInt(request.getParameter("altura"));
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        InsertarPacienteNeuDaoImpl ipudao = new InsertarPacienteNeuDaoImpl();
        ConsultaPacienteNeuDaoImpl cpudao = new ConsultaPacienteNeuDaoImpl();

        medicoNeurologia tipoMed = cpudao.findbyMed(idMed);

        session.setAttribute("tipoMed", tipoMed);
        medicoNeurologia tipoMedV = (medicoNeurologia)session.getAttribute("tipoMed");


        pacienteNeurologia pacNeu = new pacienteNeurologia(0, nom, ap1, ap2, peso, altura, tipoMedV, timestamp, dni);
        ipudao.insDatos(pacNeu);

        request.getRequestDispatcher("Neurologia/barras/barra_insertandoDatosPaciente.html").forward(request,response);
    }
    //CONSULTAR LOS DATOS DEL PACIENTE
    private void consultarDatosNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        String msj = null;

        String nom = (String) session.getAttribute("avisaNombre");
        String ap1 = (String) session.getAttribute("avisaApellido");
        int dni = (Integer)session.getAttribute("dni");

        ConsultaPacienteNeuDaoImpl cpndao = new ConsultaPacienteNeuDaoImpl();

        List<pacienteNeurologia> conNeu = new ArrayList<pacienteNeurologia>();
         conNeu = cpndao.consultarTurno(nom, ap1, dni);
        System.out.println(conNeu);
        if(conNeu.size() != 0) {
            session.setAttribute("conNeu", conNeu);
            request.getRequestDispatcher("Neurologia/ConsultarDatosNeu.jsp").forward(request, response);
        }
        else{
            msj = "Este paciente no tiene datos";
            request.setAttribute("msj", msj);
            request.getRequestDispatcher("Neurologia/ConsultaNeu.jsp").forward(request, response);
        }
    }
    // IR A SALA DE MODIFICAR DATOS
    private void irModidicarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Neurologia/ActualizarDatosNeu.jsp").forward(request, response);
    }
    //MODIFICAR LOS DATOS DEL PACIENTE ACTUAL
    private void modificarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       HttpSession session = request.getSession();

       int dni = (Integer)session.getAttribute("dni");
       double peso = Double.parseDouble(request.getParameter("peso"));
       int altura = Integer.parseInt(request.getParameter("altura"));

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        ConsultaPacienteNeuDaoImpl cpndao = new ConsultaPacienteNeuDaoImpl();
        ModificarPacienteNeuDaoImpl mpudao = new ModificarPacienteNeuDaoImpl();

        List<pacienteNeurologia> cDNI =  cpndao.conDNIPac(dni);

        int idPac = cDNI.get(0).getIdPaciente();

        pacienteNeurologia pacI = cpndao.findIdPac(idPac);
        session.setAttribute("pacI", pacI);

        pacienteNeurologia pacV = (pacienteNeurologia)session.getAttribute("pacI");

        pacV.setPeso(peso);
        pacV.setAltura(altura);
        pacV.setFechaConsulta(timestamp);

        mpudao.modDatos(pacV);
        session.setAttribute("idPac", idPac);

        request.getRequestDispatcher("Neurologia/barras/barra_actualizandoDatosPaciente.html").forward(request, response);
    }
    //VOLVER A CONSULTA
    private void volverConsulta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        int dni = (Integer)session.getAttribute("dni");

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        ConsultaPacienteNeuDaoImpl cpndao = new ConsultaPacienteNeuDaoImpl();
        ModificarPacienteNeuDaoImpl mpudao = new ModificarPacienteNeuDaoImpl();

        List<pacienteNeurologia> cDNI =  cpndao.conDNIPac(dni);

        int idPac = cDNI.get(0).getIdPaciente();

        pacienteNeurologia pacI = cpndao.findIdPac(idPac);
        session.setAttribute("pacI", pacI);

        pacienteNeurologia pacV = (pacienteNeurologia)session.getAttribute("pacI");

        pacV.setFechaConsulta(timestamp);

        mpudao.modDatos(pacV);

        request.getRequestDispatcher("Neurologia/barras/barra_volviendoConsulta.html").forward(request, response);
    }
}