import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;


/**
 * Servlet implementation class Main
 */
@WebServlet("/Main_Barra_Neurologia")
public class Main_Barra_Neurologia extends HttpServlet {
	private static final SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Main_Barra_Neurologia() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// SELECCIONAR OPCION

		String op = request.getParameter("op");


		switch (op) {


			case "barraIrSalaEsperaNeu":
				barraIrSalaNeu(request, response);
				break;
			case "YendoFormularioLlamar":
				barraFormularioLLamar(request, response);
				break;
			case "barraLlamarPaciente":
				barraLlamarPac(request, response);
				break;
			case "barraEntrarConsulta":
				barraEntrandoConsulta(request,response);
				break;
			case "barraIrInsertarDatos":
				barraIrInsertDatos(request, response);
				break;
			case "barraInsertandoDatosPac":
				barraInsertarDatosPac(request, response);
				break;
			case "barraIrConsultarDatos":
				barraIrConDatos(request, response);
				break;
			case "barraVolviendoConsulta":
				barraVolverConsulta(request, response);
				break;
			case "barraIrActualizarDatos":
				barraIrActDatos(request, response);
				break;
			case "barraActualizarDatosPac":
				barraActualizarDatosPac(request, response);
				break;
			case "barraTerminarConsulta":
				barraTerminandoConsulta(request,response);
				break;

		}
	}


	private void barraIrSalaNeu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/SalaEsperaNeu.jsp").forward(request, response);
	}
	private void barraFormularioLLamar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_preparandoLlamar.html").forward(request, response);
	}
	private void barraLlamarPac(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/SalaEsperaNeu.jsp").forward(request, response);
	}
	private void barraEntrandoConsulta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_entrandoConsulta.html").forward(request, response);
	}
	private void barraIrInsertDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_preparandoInsertarPaciente.html").forward(request, response);
	}
	private void barraIrConDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_consultandoDatosPaciente.html").forward(request, response);
	}
	private void barraInsertarDatosPac(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/ConsultaNeu.jsp").forward(request, response);
	}
	private void barraIrActDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_preparandoActualizarPaciente.html").forward(request, response);
	}
	private void barraVolverConsulta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/ConsultaNeu.jsp").forward(request, response);
	}
	private void barraTerminandoConsulta(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/barras/barra_terminandoConsulta.html").forward(request, response);
	}
	private void barraActualizarDatosPac(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("Neurologia/ConsultaNeu.jsp").forward(request, response);
	}
}
	
