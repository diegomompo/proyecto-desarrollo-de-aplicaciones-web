package beans.Neurologia;

import beans.TipoMedico;
import beans.especialidades;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the mediconeurologia database table.
 * 
 */
@Entity
@NamedQuery(name="medicoNeurologia.findAll", query="SELECT m FROM medicoNeurologia m")
public class medicoNeurologia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MEDICO")
	private int idMedico;

	private String apellido1;

	private String apellido2;

	private String nombre;

	//bi-directional many-to-one association to TipoMedico
	@ManyToOne
	@JoinColumn(name="ID_TIPO_MEDICO")
	private TipoMedico tipoMedico;

	//bi-directional many-to-one association to Pacienteneurologia
	@OneToMany(mappedBy="mediconeurologia")
	private List<pacienteNeurologia> pacienteneurologias;

	//bi-directional many-to-one association to Pacienteneurologia
	@ManyToOne
	@JoinColumn(name="ID_ESPECIALIDAD")
	private especialidades especialidades;

	public medicoNeurologia() {
	}

	public int getIdMedico() {
		return this.idMedico;
	}

	public void setIdMedico(int idMedico) {
		this.idMedico = idMedico;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoMedico getTipoMedico() {
		return this.tipoMedico;
	}

	public void setTipoMedico(TipoMedico tipoMedico) {
		this.tipoMedico = tipoMedico;
	}

	public List<pacienteNeurologia> getPacienteneurologias() {
		return this.pacienteneurologias;
	}

	public void setPacienteneurologias(List<pacienteNeurologia> pacienteneurologias) {
		this.pacienteneurologias = pacienteneurologias;
	}

	public especialidades getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(especialidades especialidades) {
		this.especialidades = especialidades;
	}

	public pacienteNeurologia addPacienteneurologia(pacienteNeurologia pacienteneurologia) {
		getPacienteneurologias().add(pacienteneurologia);
		pacienteneurologia.setMediconeurologia(this);

		return pacienteneurologia;
	}

	public pacienteNeurologia removePacienteneurologia(pacienteNeurologia pacienteneurologia) {
		getPacienteneurologias().remove(pacienteneurologia);
		pacienteneurologia.setMediconeurologia(null);

		return pacienteneurologia;
	}

}