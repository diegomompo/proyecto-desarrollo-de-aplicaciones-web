package beans.Neurologia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the pacienteneurologia database table.
 * 
 */
@Entity
@NamedQuery(name="pacienteNeurologia.findAll", query="SELECT p FROM pacienteNeurologia p")
public class pacienteNeurologia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PACIENTE")
	private int idPaciente;

	private String nombre;

	private String apellido1;

	private String apellido2;

	private double peso;

	private int altura;

	//bi-directional many-to-one association to Mediconeurologia
	@ManyToOne
	@JoinColumn(name="ID_MEDICO")
	private medicoNeurologia mediconeurologia;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="FECHA_CONSULTA")
	private Date fechaConsulta;

	private int dni;

	public pacienteNeurologia(){
		super();
	}

	public pacienteNeurologia(int idPaciente, String nombre, String apellido1, String apellido2,
							  double peso, int altura, medicoNeurologia mediconeurologia, Date fechaConsulta, int dni) {
		super();
		this.idPaciente = idPaciente;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.peso = peso;
		this.altura = altura;
		this.mediconeurologia = mediconeurologia;
		this.fechaConsulta = fechaConsulta;
		this.dni = dni;
	}


	public int getIdPaciente() {
		return this.idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public int getAltura() {
		return this.altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public Date getFechaConsulta() {
		return this.fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPeso() {
		return this.peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public medicoNeurologia getMediconeurologia() {
		return this.mediconeurologia;
	}

	public void setMediconeurologia(medicoNeurologia mediconeurologia) {
		this.mediconeurologia = mediconeurologia;
	}

	public int getDni() {
		return this.dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPaciente;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		pacienteNeurologia other = (pacienteNeurologia) obj;
		if (idPaciente != other.idPaciente)
			return false;
		return true;
	}
	@Override

	public String toString(){
		return "pacienteNeurologia [idPaciente=" + idPaciente + ", nombre=" + nombre + ", apellido1=" + apellido1
				+ ", apellido2=" + apellido2 + ", peso=" + peso + ", altura=" + altura +", mediconeurologia=" + mediconeurologia + ", fechaConsulta=" + fechaConsulta + ", dni=" + dni + "]";
	}
}