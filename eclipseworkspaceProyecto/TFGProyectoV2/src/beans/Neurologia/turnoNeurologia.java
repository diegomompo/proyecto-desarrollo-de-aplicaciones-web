package beans.Neurologia;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the turnoneurologia database table.
 * 
 */
@Entity
@NamedQuery(name="turnoNeurologia.findAll", query="SELECT t FROM turnoNeurologia t")
public class turnoNeurologia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PACIENTE_TURNO")
	private int idPacienteTurno;

	private String apellido1;

	private String apellido2;

	private String nombre;

	@Column(name="ID_MEDICO")
	private int id_medico;

	private int dni;


	public turnoNeurologia() {
		super();
	}

	public turnoNeurologia(int idPacienteTurno, String nombre, String apellido1, String apellido2, int dni, int id_medico) {
		this.idPacienteTurno = idPacienteTurno;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dni = dni;
		this.id_medico = id_medico;
	}

	public int getIdPacienteTurno() {
		return this.idPacienteTurno;
	}

	public void setIdPacienteTurno(int idPacienteTurno) {
		this.idPacienteTurno = idPacienteTurno;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDni() {
		return this.dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getIdMedico() {
		return this.id_medico;
	}

	public void setIdMedico(int dni) {
		this.id_medico = id_medico;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPacienteTurno;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		turnoNeurologia other = (turnoNeurologia) obj;
		if (idPacienteTurno != other.idPacienteTurno)
			return false;
		return true;
	}
	@Override

	public String toString(){
		return "turnoNeurologia [idPacienteTurno=" + idPacienteTurno + ", nombre=" + nombre + ", apellido1=" + apellido1
				+ ", apellido2=" + apellido2 + ", dni=" + dni +  ", id_medico=" + id_medico +"]";
	}

}
