package beans;

import beans.Neurologia.medicoNeurologia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_medico database table.
 * 
 */
@Entity
@Table(name="tipo_medico")
@NamedQuery(name="TipoMedico.findAll", query="SELECT t FROM TipoMedico t")
public class TipoMedico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TIPO_MEDICO")
	private int idTipoMedico;

	private String descripcion;

	@Column(name="NOMBRE_TIPO_MEDICO")
	private String nombreTipoMedico;

	//bi-directional many-to-one association to Mediconeurologia
	@OneToMany(mappedBy="tipoMedico")
	private List<medicoNeurologia> mediconeurologias;

	public TipoMedico() {
	}

	public int getIdTipoMedico() {
		return this.idTipoMedico;
	}

	public void setIdTipoMedico(int idTipoMedico) {
		this.idTipoMedico = idTipoMedico;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreTipoMedico() {
		return this.nombreTipoMedico;
	}

	public void setNombreTipoMedico(String nombreTipoMedico) {
		this.nombreTipoMedico = nombreTipoMedico;
	}


	public List<medicoNeurologia> getMediconeurologias() {
		return this.mediconeurologias;
	}

	public void setMediconeurologias(List<medicoNeurologia> mediconeurologias) {
		this.mediconeurologias = mediconeurologias;
	}

	public medicoNeurologia addMediconeurologia(medicoNeurologia mediconeurologia) {
		getMediconeurologias().add(mediconeurologia);
		mediconeurologia.setTipoMedico(this);

		return mediconeurologia;
	}

	public medicoNeurologia removeMediconeurologia(medicoNeurologia mediconeurologia) {
		getMediconeurologias().remove(mediconeurologia);
		mediconeurologia.setTipoMedico(null);

		return mediconeurologia;
	}

}