package beans;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.pacienteNeurologia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_medico database table.
 * 
 */
@Entity
@Table(name="especialidades")
@NamedQuery(name="especialidades.findAll", query="SELECT e FROM especialidades e")
public class especialidades implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ESPECIALIDAD")
	private int idEspecialidad;

	private String especialidad;

	//bi-directional many-to-one association to Pacienteneurologia
	@OneToMany(mappedBy="especialidades")
	private List<medicoNeurologia> medicoNeurologia;

	public especialidades() {
	}

	public int getIdEspecialidad() {
		return this.idEspecialidad;
	}

	public void setIdEspecialidad(int idTipoMedico) {
		this.idEspecialidad = idEspecialidad;
	}

	public String getEspecialidad() {
		return this.especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public List<beans.Neurologia.medicoNeurologia> getMedicoNeurologia() {
		return medicoNeurologia;
	}

	public void setMedicoNeurologia(List<beans.Neurologia.medicoNeurologia> medicoNeurologia) {
		this.medicoNeurologia = medicoNeurologia;
	}

	public medicoNeurologia addMediconeurologia(medicoNeurologia medicoNeurologia) {
		getMedicoNeurologia().add(medicoNeurologia);
		medicoNeurologia.setEspecialidades(this);
		return medicoNeurologia;
	}

	public medicoNeurologia removeMediconeurologia(medicoNeurologia medicoNeurologia) {
		getMedicoNeurologia().remove(null);
		return medicoNeurologia;
	}

}