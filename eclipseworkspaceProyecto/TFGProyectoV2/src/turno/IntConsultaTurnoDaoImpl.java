package turno;

import java.util.List;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.turnoNeurologia;
import beans.TipoMedico;
import beans.especialidades;

public interface IntConsultaTurnoDaoImpl {
	List<turnoNeurologia> findbyNombreAp(String nombre, String apellido, int dni);
	List<medicoNeurologia> medico(TipoMedico tipo_medico);
	TipoMedico findbyTMed(int tipo_medico);
	List<medicoNeurologia> findByMed(int id_medico);
}
