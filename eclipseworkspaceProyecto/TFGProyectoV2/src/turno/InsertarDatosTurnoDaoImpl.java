package turno;

import beans.Neurologia.turnoNeurologia;
import dao.AbstractClinicaDao;

public class InsertarDatosTurnoDaoImpl extends AbstractClinicaDao implements IntInsertarDatosTurnoDaoImpl {

	@Override
	public int insertTurn(turnoNeurologia turno) {
		int filasTurno = 0;
		
		try {
			tx.begin();
			em.persist(turno);
			tx.commit();
			filasTurno = 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return filasTurno;
	}

}
