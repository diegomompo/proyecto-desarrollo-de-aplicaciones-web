package turno;

import beans.Neurologia.pacienteNeurologia;
import beans.Neurologia.turnoNeurologia;

public interface IntBorrarTurnoDaoImpl {
	turnoNeurologia findbyNum(int idPacTurno);
	int deleteTurno(turnoNeurologia turnNeu);
}
