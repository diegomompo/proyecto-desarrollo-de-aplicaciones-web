package turno;

import beans.Neurologia.pacienteNeurologia;
import beans.Neurologia.turnoNeurologia;
import turno.ConsultaTurnoDaoImpl;
import dao.AbstractClinicaDao;
import pacienteNeu.IntModificarPacienteNeuDaoImpl;

public class BorrarTurnoDaoImpl extends AbstractClinicaDao implements IntBorrarTurnoDaoImpl{

	@Override
	public turnoNeurologia findbyNum(int idPacTurno) {
		return em.find(turnoNeurologia.class, idPacTurno);
	}

	@Override
	public int deleteTurno(turnoNeurologia turnNeu) {
		int filasTurnNeu = 0;
		if (findbyNum(turnNeu.getIdPacienteTurno()) != null) {
			try {
				tx.begin();
				em.remove(turnNeu);
				tx.commit();
				filasTurnNeu = 1;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return filasTurnNeu;
	}
}
