package turno;

import java.util.List;

import beans.Neurologia.medicoNeurologia;
import beans.Neurologia.pacienteNeurologia;
import beans.Neurologia.turnoNeurologia;
import beans.TipoMedico;
import beans.especialidades;
import dao.AbstractClinicaDao;

public class ConsultaTurnoDaoImpl extends AbstractClinicaDao implements IntConsultaTurnoDaoImpl {

	@Override
	public List<turnoNeurologia> findbyNombreAp(String nombre, String apellido, int dni) {
		sql = "SELECT t FROM turnoNeurologia t WHERE t.nombre = :nom AND t.apellido1 = :ap1 AND t.dni = :DNI";
		query = em.createQuery(sql);
		query.setParameter("nom", nombre);
		query.setParameter("ap1", apellido);
		query.setParameter("DNI", dni);
		return query.getResultList();
	}
	public List<medicoNeurologia> medico(TipoMedico tipo_medico) {
		sql = "SELECT m FROM medicoNeurologia m WHERE m.tipoMedico = :tMedico";
		query = em.createQuery(sql);
		query.setParameter("tMedico", tipo_medico);
		return query.getResultList();
	}

	public List<turnoNeurologia> conDNIPac(int dni) {
		sql = "SELECT p FROM turnoNeurologia p WHERE  p.dni = :DNI";
		query = em.createQuery(sql);
		query.setParameter("DNI", dni);
		return query.getResultList();
	}
	@Override

	public TipoMedico findbyTMed(int tipo_medico) {return em.find(TipoMedico.class, tipo_medico);}

	public List<medicoNeurologia> findByMed(int id_medico) {
		sql = "SELECT m FROM medicoNeurologia m WHERE m.idMedico = :idMed";
		query = em.createQuery(sql);
		query.setParameter("idMed", id_medico);
		return query.getResultList();
	}

}
