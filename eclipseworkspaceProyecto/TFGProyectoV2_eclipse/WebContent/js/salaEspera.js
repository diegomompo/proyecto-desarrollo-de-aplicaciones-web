window.onload = function(){
	$(document).ready(esperaSala);
}
function esperaSala() {
	let divEspera = $("<div>");
	divEspera.attr("id", "esp");
	
	$("body").append(divEspera);
	
	jsonEspera();
}

// LLAMADA A JSON
function jsonEspera(){
	$.ajax({
		'type' : 'GET',
		'url' : "./json/salaEspera.json",
		"aysnc": true,
	}).done(esperandoSala);
}

function esperandoSala(objetoJSONEspera){
	arrayEspera = objetoJSONEspera.espSala;
	arrayLlamar = objetoJSONEspera.llamarPac;
	arrayEntrar = objetoJSONEspera.entrarCon;
	
	$.each(arrayEspera, function(i, espSala){
	$.each(arrayLlamar, function(i, llamarPac){
	$.each(arrayEntrar, function(i, entrarCon){
		
		//TÍTULO
		let elementH1 = $(espSala.elementh1);
		elementH1.text(espSala.titleh1);
		
		//BOTON LLAMAR AL PACIENTE
		let elementFormLlamarP = $(espSala.elementForm);
		elementFormLlamarP.attr("action", llamarPac.actionFormLlamarP);
		elementFormLlamarP.attr("method", espSala.method);
		let elementButtonLlamarP = $(espSala.elementButton);
		elementButtonLlamarP.attr("type", espSala.typeButton);
		elementButtonLlamarP.attr("class", espSala.classButton);
		elementButtonLlamarP.click(llamaPaciente);
		elementButtonLlamarP.text(llamarPac.titleButton);
		
		//BOTON ENTRAR A LA CONSULTA
		let elementFormEntrarC = $(espSala.elementForm);
		elementFormEntrarC.attr("action", entrarCon.actionFormEntrarC);
		elementFormEntrarC.attr("method", espSala.method);
		let elementButtonEntrarC = $(espSala.elementButton);
		elementButtonEntrarC.attr("type", espSala.typeButton);
		elementButtonEntrarC.attr("class", espSala.classButton);
		elementButtonEntrarC.text(entrarCon.titleButton);
		
		//IMPRIMIR PARA MOSTRARLO EN EL JSP
		$("#esp").append(elementH1);
		
		$("#esp").append(elementFormLlamarP);
		$(elementFormLlamarP).append(elementButtonLlamarP);
		
		$("#esp").append(elementFormEntrarC);
		$(elementFormEntrarC).append(elementButtonEntrarC);
	})	
	})	
	})
	
}
/**
 * 
 */