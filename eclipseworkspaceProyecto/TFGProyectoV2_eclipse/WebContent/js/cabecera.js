window.onload = function(){
	headPrograma();
}

//  LLAMADA A AL JSON DE LA CABECEERA
function headPrograma(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/cabecera.json",
		"aysnc": true,
		}
	).done(cabeceraPrograma)
}
function cabeceraPrograma(objetoJSONCabecera){
	arrayHead = objetoJSONCabecera.cabeceraInicio;
	
	//HEAD DE LA PÁGINA
	
	$.each(arrayHead, function(i, cabeceraInicio){
		
		let elementMeta = $(cabeceraInicio.elementmeta);
		elementMeta.attr("charset", cabeceraInicio.charset);
		
		let elementTitle = $(cabeceraInicio.elementtitle);
		elementTitle.text(cabeceraInicio.title);
		
		$("head").append(elementMeta);
		$("head").append(elementTitle);
		
	})
}


/**
 * 
 */