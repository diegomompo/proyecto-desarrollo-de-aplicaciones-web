window.onload = function(){
	$(document).ready(entrarClinica);
}
function entrarClinica(){
	let divEntrar = $("<div>");
	divEntrar.attr("id", "ent");
	
	$("body").append(divEntrar);
	
	jsonEntrar();
}

//LLAMADA A JSON
function jsonEntrar(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/entrar.json",
		"aysnc": true,
		}
	).done(entrandoClinica)
}
function entrandoClinica(objetoJSONEntrar){
	arrayEntrar = objetoJSONEntrar.entrando;
	arrayTurno = objetoJSONEntrar.solicitarTurno;
	arraySalaEspera = objetoJSONEntrar.irSalaEspera;
	
	
	$.each(arrayEntrar, function(i, entrando){
		$.each(arrayTurno, function(i, solicitarTurno){
			$.each(arraySalaEspera, function(i, irSalaEspera){
				
				//TITULO
				let elementh1 = $(entrando.elementH1);
				elementh1.text(entrando.titleH1);
				
				//BOTON PARA SOLICITAR EL TURNO
				let elementFormTurno = $(entrando.elementForm);
				elementFormTurno.attr("action", solicitarTurno.actionFormTurno);
				elementFormTurno.attr("method", entrando.method);
				let elementButtonTurno = $(entrando.elementButton);
				elementButtonTurno.attr("type", entrando.typeButton);
				elementButtonTurno.attr("class", entrando.classButton);
				elementButtonTurno.text(solicitarTurno.titleButton);
				
				//BOTON PARA IR A LA SALA DE ESPERA
				let elementFormEspera = $(entrando.elementForm);
				elementFormEspera.attr("action", irSalaEspera.actionFormEspera);
				elementFormEspera.attr("method", entrando.method);
				let elementButtonEspera = $(entrando.elementButton);
				elementButtonEspera.attr("type", entrando.typeButton);
				elementButtonEspera.attr("class", entrando.classButton);
				elementButtonEspera.text(irSalaEspera.titleButton);
				
				//IMPRIMIR PARA MOSTRARLO EN JSP
				$("#ent").append(elementh1);
				$("#ent").append(elementFormTurno);
				$(elementFormTurno).append(elementButtonTurno);
				$("#ent").append(elementFormEspera);
				$(elementFormEspera).append(elementButtonEspera);
			})
		})
	})
}
/**
 * 
 */