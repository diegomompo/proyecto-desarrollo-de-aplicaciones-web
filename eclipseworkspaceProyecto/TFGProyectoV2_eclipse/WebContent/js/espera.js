window.onload = function(){
	$(document).ready(consultaTurno);
}
function consultaTurno(){
	let divEspera = $("<div>");
	divEspera.attr("id", "esp");
	$("body").append(divEspera);
	
	EsperaJSON();
}
function EsperaJSON(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/espera.json",
		"async": true
		}
	).done(datosEspera)
}
function datosEspera(objetoJSONDatos){
	
	arrayInsertar = objetoJSONDatos.registrarDatos;
	arrayNombre = objetoJSONDatos.datosNombre;
	arrayApellido1 = objetoJSONDatos.datosApellido1;
	arrayApellido2 = objetoJSONDatos.datosApellido2;
	arrayEspecialista = objetoJSONDatos.datosEspecialista;
	arrayVolverInicio = objetoJSONDatos.volverInicio;
	
	$.each(arrayInsertar, function(i, registrarDatos){
	$.each(arrayNombre, function(i, datosNombre){
	$.each(arrayApellido1, function(i, datosApellido1){
	$.each(arrayApellido2, function(i, datosApellido2){
	$.each(arrayEspecialista, function(i, datosEspecialista){
	$.each(arrayVolverInicio, function(i, volverInicio){

		//TITULO
		let elementh1 = $(registrarDatos.elementH1);
		elementh1.text(registrarDatos.title1);
		
		//FORMULARIO
		let elementForm = $(registrarDatos.elementFORM);
		elementForm.attr("action", registrarDatos.actionFORM);
		elementForm.attr("method", registrarDatos.methodFORM);
		
		//INSERTAR EL NOMBRE
		let elementDivNombre = $(registrarDatos.elementDIV);
		elementDivNombre.attr("class", registrarDatos.classDIV);
		
		let elementLabelNombre = $(registrarDatos.elementLABEL);
		elementLabelNombre.attr("for", datosNombre.forNombre);
		elementLabelNombre.text(datosNombre.titleNombre);
		
		let elementInputNombre = $(registrarDatos.elementINPUT);
		elementInputNombre.attr("type", registrarDatos.typeINPUT);
		elementInputNombre.attr("name", datosNombre.nameNombre);
		elementInputNombre.attr("class", registrarDatos.classINPUT);
		elementInputNombre.attr("id", datosNombre.idNombre);
		
		//INSERTAR EL PRIMER APELLIDO
		let elementDivAp1 = $(registrarDatos.elementDIV);
		elementDivAp1.attr("class", registrarDatos.classDIV);
		
		let elementLabelAp1 = $(registrarDatos.elementLABEL);
		elementLabelAp1.attr("for", datosApellido1.forApellido1);
		elementLabelAp1.text(datosApellido1.titleApellido1);
		
		let elementInputAp1 = $(registrarDatos.elementINPUT);
		elementInputAp1.attr("type", registrarDatos.typeINPUT);
		elementInputAp1.attr("name", datosApellido1.nameApellido1);
		elementInputAp1.attr("class", registrarDatos.classINPUT);
		elementInputAp1.attr("id", datosApellido1.idApellido1);
		
		//INSERTAR EL SEGUNDO APELLIDO
		let elementDivAp2 = $(registrarDatos.elementDIV);
		elementDivAp2.attr("class", registrarDatos.classDIV);
		
		let elementLabelAp2 = $(registrarDatos.elementLABEL);
		elementLabelAp2.attr("for", datosApellido2.forApellido2);
		elementLabelAp2.text(datosApellido2.titleApellido2);
		
		let elementInputAp2 = $(registrarDatos.elementINPUT);
		elementInputAp2.attr("type", registrarDatos.typeINPUT);
		elementInputAp2.attr("name", datosApellido2.nameApellido2);
		elementInputAp2.attr("class", registrarDatos.classINPUT);
		elementInputAp2.attr("id", datosApellido2.idApellido2);

		//INSERTAR EL CAMPO ESPECIALISTA
		let elementDivEspec = $(registrarDatos.elementDIV);
		elementDivEspec.attr("class", registrarDatos.classDIV);
		
		let elementLabelEspec = $(registrarDatos.elementLABEL);
		elementLabelEspec.attr("for", datosEspecialista.forEspecialista);
		elementLabelEspec.text(datosEspecialista.titleEspecialista);
		
		let elementInputEspec = $(registrarDatos.elementINPUT);
		elementInputEspec.attr("type", registrarDatos.typeINPUT);
		elementInputEspec.attr("name", datosEspecialista.nameEspecialista);
		elementInputEspec.attr("class", registrarDatos.classINPUT);
		elementInputEspec.attr("id", datosEspecialista.idEspecialista);
		
		//BOTON DE REGISTRAR
		let elementButton = $(registrarDatos.elementBUTTON);
		elementButton.attr("type", registrarDatos.typeButton);
		elementButton.attr("class", registrarDatos.classButton);
		elementButton.text(registrarDatos.titleButton);
		
		//ACCIÓN PARA VOLVER
		let elementFVolver = $(volverInicio.elementFormVolver);
		elementFVolver.attr("action", volverInicio.actionFormVolver);
		elementFVolver.attr("method", volverInicio.methodFormVolver);
		
		//BOTÓN VOLVER
		
		let elementBVolver = $(volverInicio.elementButtonVolver);
		elementBVolver.attr("type", volverInicio.typeButtonVolver);
		elementBVolver.attr("class", volverInicio.classButtonVolver);
		elementBVolver.text(volverInicio.titleButtonVolver);
		
		//IMPRIMIR TITULO POR PANTALLA
		$("#esp").append(elementh1);
		
		//IMPRIMIR FORMUALRIO POR PANTALLA
		$("#esp").append(elementForm);
		
		//IMPRIMIR CAMPO NOMBRE POR PANTALLA
		$(elementForm).append(elementDivNombre);
		$(elementDivNombre).append(elementLabelNombre);
		$(elementDivNombre).append(elementInputNombre);
		
		//IMPRIMIR CAMPO PRIMER APELLIDO POR PANTALLA
		$(elementForm).append(elementDivAp1);
		$(elementDivAp1).append(elementLabelAp1);
		$(elementDivAp1).append(elementInputAp1);
		
		//IMPRIMIR CAMPO SEGUNDO APELLIDO POR PANTALLA
		$(elementForm).append(elementDivAp2);
		$(elementDivAp2).append(elementLabelAp2);
		$(elementDivAp2).append(elementInputAp2);
		
		//IMPRIMIR CAMPO ESPECIALISTA POR PANTALLA
		$(elementForm).append(elementDivEspec);
		$(elementDivEspec).append(elementLabelEspec);
		$(elementDivEspec).append(elementInputEspec);
	
		
		//IMPRIMIR BOTÓN REGISTRAR
		$(elementForm).append(elementButton);
		
		//IMPRIMIR ACCIÓN VOLVER POR PANTALLA
		$("#esp").append(elementFVolver);
		
		//IMPRIMIR BOTÓN VOLVER
		$(elementFVolver).append(elementBVolver);
		
	})
	})
	})
	})
	})
	})
	
}

/**
 * 
 */