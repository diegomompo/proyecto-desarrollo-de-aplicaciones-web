function formuForDatos(){
	let divTurno = $("<div>");
	$("body").append(divTurno);
	
	datosJSON();
}
function datosJSON(){
	$.ajax({
		'type': 'GET',
		'url' : "./json/formu.json",
		"async": true
		}
	).done(FormuDatosTurno)
}
function formuDatosTurno(objetoJSONDatos){
	
	arrayInsertar = objetoJSONDatos.insertarDatos;
	arrayNombre = objetoJSONDatos.datosNombre;
	arrayApellido1 = objetoJSONDatos.datosApellido1;
	arrayApellido2 = objetoJSONDatos.datosApellido2;
	arrayEspecialista = objetoJSONDatos.datosEspecialista;
	
	$.each(arrayInsertar, function(i, insertarDatos){
	$.each(arrayNombre, function(i, datosNombre){
	$.each(arrayApellido1, function(i, datosApellido1){
	$.each(arrayApellido2, function(i, datosApellido2){
	$.each(arrayEspecialista, function(i, datosEspecialista){
	
	//INSERTAR EL NOMBRE
	let elementDivNombre = $(insertarDatos.elementDIV);
	elementDivNombre.attr("class", insertarDatos.classDIV);
	
	let elementLabelNombre = $(insertarDatos.elementLABEL);
	elementLabelNombre.attr("for", datosNombre.forNombre);
	elementLabelNombre.text(datosNombre.titleNombre);
	
	let elementInputNombre = $(insertarDatos.elementINPUT);
	elementInputNombre.attr("type", insertarDatos.typeINPUT);
	elementInputNombre.attr("name", datosNombre.nameNombre);
	elementInputNombre.attr("class", insertarDatos.classINPUT);
	elementInputNombre.attr("id", datosNombre.idNombre);
	
	//INSERTAR EL PRIMER APELLIDO
	let elementDivAp1 = $(registrarDatos.elementDIV);
	elementDivAp1.attr("class", registrarDatos.classDIV);
	
	let elementLabelAp1 = $(insertarDatos.elementLABEL);
	elementLabelAp1.attr("for", datosApellido1.forApellido1);
	elementLabelAp1.text(datosApellido1.titleApellido1);
	
	let elementInputAp1 = $(insertarDatos.elementINPUT);
	elementInputAp1.attr("type", insertarDatos.typeINPUT);
	elementInputAp1.attr("name", datosApellido1.nameApellido1);
	elementInputAp1.attr("class", insertarDatos.classINPUT);
	elementInputAp1.attr("id", datosApellido1.idApellido1);
	
	//INSERTAR EL SEGUNDO APELLIDO
	let elementDivAp2 = $(insertarDatos.elementDIV);
	elementDivAp2.attr("class", insertarDatos.classDIV);
	
	let elementLabelAp2 = $(insertarDatos.elementLABEL);
	elementLabelAp2.attr("for", datosApellido2.forApellido2);
	elementLabelAp2.text(datosApellido2.titleApellido2);
	
	let elementInputAp2 = $(insertarDatos.elementINPUT);
	elementInputAp2.attr("type", insertarDatos.typeINPUT);
	elementInputAp2.attr("name", datosApellido2.nameApellido2);
	elementInputAp2.attr("class", insertarDatos.classINPUT);
	elementInputAp2.attr("id", datosApellido2.idApellido2);

	//INSERTAR EL CAMPO ESPECIALISTA
	let elementDivEspec = $(insertarDatos.elementDIV);
	elementDivEspec.attr("class", insertarDatos.classDIV);
	
	let elementLabelEspec = $(insertarDatos.elementLABEL);
	elementLabelEspec.attr("for", datosEspecialista.forEspecialista);
	elementLabelEspec.text(datosEspecialista.titleEspecialista);
	
	let elementInputEspec = $(insertarDatos.elementINPUT);
	elementInputEspec.attr("type", insertarDatos.typeINPUT);
	elementInputEspec.attr("name", datosEspecialista.nameEspecialista);
	elementInputEspec.attr("class", insertarDatos.classINPUT);
	elementInputEspec.attr("id", datosEspecialista.idEspecialista);	
	

	//IMPRIMIR CAMPO NOMBRE POR PANTALLA
	$("#formu").append(elementDivNombre);
	$(elementDivNombre).append(elementLabelNombre);
	$(elementDivNombre).append(elementInputNombre);
	
	//IMPRIMIR CAMPO PRIMER APELLIDO POR PANTALLA
	$("#formu").append(elementDivAp1);
	$(elementDivAp1).append(elementLabelAp1);
	$(elementDivAp1).append(elementInputAp1);
	
	//IMPRIMIR CAMPO SEGUNDO APELLIDO POR PANTALLA
	$("#formu").append(elementDivAp2);
	$(elementDivAp2).append(elementLabelAp2);
	$(elementDivAp2).append(elementInputAp2);
	
	//IMPRIMIR CAMPO ESPECIALISTA POR PANTALLA
	$("#formu").append(elementDivEspec);
	$(elementDivEspec).append(elementLabelEspec);
	$(elementDivEspec).append(elementInputEspec);
	
	})
	})
	})
	})
	})
}


/**
 * 
 */