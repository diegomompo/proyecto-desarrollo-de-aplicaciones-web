package turno;

import java.util.List;

import beans.TipoEspecialista;
import beans.Turno;

public interface IntConsultaTurnoDaoImpl {
	List<Turno> findbyNombreAp(String nombre, String apellido, int idEspec);
	TipoEspecialista findbyEspec(String espec);
	Turno findbyNum(int numTurn);
	List<Turno> findbyNombreAp2(String nombre, String apellido);
}
