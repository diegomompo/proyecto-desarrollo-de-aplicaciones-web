package turno;

import java.util.List;

import beans.TipoEspecialista;
import beans.Turno;
import dao.AbstractClinicaDao;

public class ConsultaTurnoDaoImpl extends AbstractClinicaDao implements IntConsultaTurnoDaoImpl {

	@Override
	public List<Turno> findbyNombreAp(String nombre, String apellido, int idEspec) {
		sql = "SELECT t FROM Turno t WHERE t.turno.nombre = :nom AND t.turno.apellido1 =: ap1 AND t.tipo_especialista.idTipoEspecialista =. espec";
		query = em.createQuery(sql);
		query.setParameter("nom", nombre);
		query.setParameter("ap1", apellido);
		query.setParameter("espec", idEspec);
		return query.getResultList();
	}

	@Override
	public TipoEspecialista findbyEspec(String espec) {
		return em.find(TipoEspecialista.class, espec);
	}

	@Override
	public Turno findbyNum(int numTurn) {
		return em.find(Turno.class, numTurn);
	}
	
	public List<Turno> findbyNombreAp2(String nombre, String apellido) {
		sql = "SELECT t FROM Turno t WHERE t.turno.nombre = :nom AND t.turno.apellido1 =: ap1";
		query = em.createQuery(sql);
		query.setParameter("nom", nombre);
		query.setParameter("ap1", apellido);
		return query.getResultList();
	}


}
