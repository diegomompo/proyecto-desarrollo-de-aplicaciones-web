package turno;

import beans.Turno;

public interface IntInsertarDatosTurnoDaoImpl {
	int insertTurn(Turno turno);
	
}
