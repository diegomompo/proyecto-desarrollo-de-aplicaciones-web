package beans;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the turno database table.
 * 
 */
@Entity
@NamedQuery(name="Turno.findAll", query="SELECT t FROM Turno t")
public class Turno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PACIENTE_TURNO")
	private int idPacienteTurno;

	private String apellido1;

	private String apellido2;

	@ManyToOne
	@JoinColumn(name="ID_ESPECIALISTA")
	private int idEspecialista;

	private String nombre;

	@Column(name="NUMERO_TURNO")
	private int numeroTurno;

	public Turno() {
	}

	public int getIdPacienteTurno() {
		return this.idPacienteTurno;
	}

	public void setIdPacienteTurno(int idPacienteTurno) {
		this.idPacienteTurno = idPacienteTurno;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public int getIdEspecialista() {
		return this.idEspecialista;
	}

	public void setIdEspecialista(int idEspecialista) {
		this.idEspecialista = idEspecialista;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumeroTurno() {
		return this.numeroTurno;
	}

	public void setNumeroTurno(int numeroTurno) {
		this.numeroTurno = numeroTurno;
	}

}