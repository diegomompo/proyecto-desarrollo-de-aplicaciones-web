package beans;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_medico database table.
 * 
 */
@Entity
@Table(name="tipo_medico")
@NamedQuery(name="TipoMedico.findAll", query="SELECT t FROM TipoMedico t")
public class TipoMedico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TIPO_MEDICO")
	private int idTipoMedico;

	private String descripcion;

	@Column(name="NOMBRE_TIPO_MEDICO")
	private String nombreTipoMedico;

	//bi-directional many-to-one association to Medico
	@OneToMany(mappedBy="tipoMedico")
	private List<Medico> medicos;

	public TipoMedico() {
	}

	public int getIdTipoMedico() {
		return this.idTipoMedico;
	}

	public void setIdTipoMedico(int idTipoMedico) {
		this.idTipoMedico = idTipoMedico;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreTipoMedico() {
		return this.nombreTipoMedico;
	}

	public void setNombreTipoMedico(String nombreTipoMedico) {
		this.nombreTipoMedico = nombreTipoMedico;
	}

	public List<Medico> getMedicos() {
		return this.medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	public Medico addMedico(Medico medico) {
		getMedicos().add(medico);
		medico.setTipoMedico(this);

		return medico;
	}

	public Medico removeMedico(Medico medico) {
		getMedicos().remove(medico);
		medico.setTipoMedico(null);

		return medico;
	}

}