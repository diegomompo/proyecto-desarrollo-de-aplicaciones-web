package beans;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the paciente database table.
 * 
 */
@Entity
@NamedQuery(name="Paciente.findAll", query="SELECT p FROM Paciente p")
public class Paciente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PACIENTE")
	private int idPaciente;

	private int altura;

	private String apellido1;

	private String apellido2;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_CONSULTA")
	private Date fechaConsulta;

	private String nombre;

	private int peso;

	//bi-directional many-to-one association to Medico
	@ManyToOne
	@JoinColumn(name="ID_MEDICO")
	private Medico medico;

	//bi-directional many-to-one association to TipoEspecialista
	@ManyToOne
	@JoinColumn(name="ID_TIPO_ESPECIALISTA")
	private TipoEspecialista tipoEspecialista;

	public Paciente() {
	}

	public int getIdPaciente() {
		return this.idPaciente;
	}

	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}

	public int getAltura() {
		return this.altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public Date getFechaConsulta() {
		return this.fechaConsulta;
	}

	public void setFechaConsulta(Date fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPeso() {
		return this.peso;
	}

	public void setPeso(int peso) {
		this.peso = peso;
	}

	public Medico getMedico() {
		return this.medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public TipoEspecialista getTipoEspecialista() {
		return this.tipoEspecialista;
	}

	public void setTipoEspecialista(TipoEspecialista tipoEspecialista) {
		this.tipoEspecialista = tipoEspecialista;
	}

}