package beans;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medico database table.
 * 
 */
@Entity
@NamedQuery(name="Medico.findAll", query="SELECT m FROM Medico m")
public class Medico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MEDICO")
	private int idMedico;

	private String apellido1;

	private String apellido2;

	private String nombre;

	//bi-directional many-to-one association to TipoEspecialista
	@ManyToOne
	@JoinColumn(name="ID_TIPO_ESPECIALISTA")
	private TipoEspecialista tipoEspecialista;

	//bi-directional many-to-one association to TipoMedico
	@ManyToOne
	@JoinColumn(name="ID_TIPO_MEDICO")
	private TipoMedico tipoMedico;

	//bi-directional many-to-one association to Paciente
	@OneToMany(mappedBy="medico")
	private List<Paciente> pacientes;

	public Medico() {
	}

	public int getIdMedico() {
		return this.idMedico;
	}

	public void setIdMedico(int idMedico) {
		this.idMedico = idMedico;
	}

	public String getApellido1() {
		return this.apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return this.apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoEspecialista getTipoEspecialista() {
		return this.tipoEspecialista;
	}

	public void setTipoEspecialista(TipoEspecialista tipoEspecialista) {
		this.tipoEspecialista = tipoEspecialista;
	}

	public TipoMedico getTipoMedico() {
		return this.tipoMedico;
	}

	public void setTipoMedico(TipoMedico tipoMedico) {
		this.tipoMedico = tipoMedico;
	}

	public List<Paciente> getPacientes() {
		return this.pacientes;
	}

	public void setPacientes(List<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	public Paciente addPaciente(Paciente paciente) {
		getPacientes().add(paciente);
		paciente.setMedico(this);

		return paciente;
	}

	public Paciente removePaciente(Paciente paciente) {
		getPacientes().remove(paciente);
		paciente.setMedico(null);

		return paciente;
	}

}