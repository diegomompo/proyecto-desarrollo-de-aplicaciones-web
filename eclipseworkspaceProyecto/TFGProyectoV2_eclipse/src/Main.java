

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TipoEspecialista;
import beans.Turno;
import turno.ConsultaTurnoDaoImpl;
import turno.InsertarDatosTurnoDaoImpl;


/**
 * Servlet implementation class Main
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final SimpleDateFormat fecha = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Main() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// SELECCIONAR OPCION
		
		String op = request.getParameter("op");
	
		
		switch(op) {
		
		
			case "solicitandoTurno":
				pedirTurno(request, response);
			break;
			case "insertandoDatos":
				insertarDatos(request, response);
			break;
			case "irSala":
				yendoSalaEspera(request, response);
			break;
			case "consultandoTurno":
				consultarTurno(request, response);
			break;
			case "volviendoInicio":
				volverInicio(request, response);
			break;
			case "llamandoPaciente":
				llamarPaciente(request, response);
			break;
		}
	}

	private void pedirTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		request.getRequestDispatcher("Turno.jsp").forward(request, response);
	}

	// REGISTRANDO EL TURNO
	private void insertarDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		String nom = request.getParameter("nombre");
		String ap1 = request.getParameter("apellido1");
		String ap2 = request.getParameter("apellido2");
		String espec = request.getParameter("especialista");
		int numTurn = Integer.parseInt(request.getParameter("numero_turno"));
		
		
		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		InsertarDatosTurnoDaoImpl idtdao = new InsertarDatosTurnoDaoImpl();
		
		TipoEspecialista espTipo = ctdao.findbyEspec(espec);
		
		System.out.println(espTipo);
		
		if(espTipo != null){
			session.setAttribute("espTipo", espTipo);
			TipoEspecialista tipoEsp = (TipoEspecialista)session.getAttribute("espTipo");
			
			int idEspec = tipoEsp.getIdTipoEspecialista();
			
			System.out.println(tipoEsp.getIdTipoEspecialista());
			
		}else {
			msj = "El tipo de especialista no existe";
			request.setAttribute("msj", msj);
			request.getRequestDispatcher("Turno.jsp").forward(request, response);
		}	
	}
	
	//IR A LA SALA DE ESPERA

	private void yendoSalaEspera(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
			request.getRequestDispatcher("ConsultaEspera.jsp").forward(request, response);
	}
	
	private void consultarTurno(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String msj = null;
		
		String nom = request.getParameter("nombre");
		String ap1 = request.getParameter("apellido1");
		String ap2 = request.getParameter("apellido2");
		String espec = request.getParameter("especialista");
		
		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		InsertarDatosTurnoDaoImpl idtdao = new InsertarDatosTurnoDaoImpl();
		
		TipoEspecialista espTipo = ctdao.findbyEspec(espec);
		session.setAttribute("espTipo", espTipo);
			
			TipoEspecialista tipoEsp = (TipoEspecialista)session.getAttribute("espTipo");
			List<Turno> tExiste = ctdao.findbyNombreAp(nom, ap1, tipoEsp.getIdTipoEspecialista());
		
			if(tExiste != null) {
					System.out.println("Est� dentro");
			}else {
				msj = "El paciente introducirdo no tiene Turno. Por favor introduzca";
				request.setAttribute("msj", msj);
				request.getRequestDispatcher("ConsultaEspera.jsp").forward(request, response);
			}
	}
	
	private void volverInicio(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.getRequestDispatcher("Inicio.jsp").forward(request, response);
	}
	
	// LLAMAR A UN PACIENTE
	
	private void llamarPaciente(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sesion = request.getSession();
		
		Turno turno = (Turno)sesion.getAttribute("tExiste");
		
		ConsultaTurnoDaoImpl ctdao = new ConsultaTurnoDaoImpl();
		
		List<Turno> Paciente = ctdao.findbyNombreAp2(turno.getNombre(), turno.getApellido1());
		
		sesion.setAttribute("lPaciente", "lPaciente");
		request.getRequestDispatcher("SalaEspera.jsp").forward(request, response);
		
		
	}
}
