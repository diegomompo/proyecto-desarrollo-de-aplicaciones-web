package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

public abstract class AbstractClinicaDao {
	
	EntityManagerFactory emf;
	protected EntityManager em;
	protected EntityTransaction tx;
	protected Query query;
	protected String sql;
	
	public  AbstractClinicaDao() {
		emf = Persistence.createEntityManagerFactory("TFGProyectoV2");
		em = emf.createEntityManager();
		tx = em.getTransaction();

	}

}
