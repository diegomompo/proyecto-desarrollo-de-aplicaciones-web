package beans;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_especialista database table.
 * 
 */
@Entity
@Table(name="tipo_especialista")
@NamedQuery(name="TipoEspecialista.findAll", query="SELECT t FROM TipoEspecialista t")
public class TipoEspecialista implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TIPO_ESPECIALISTA")
	private int idTipoEspecialista;

	private String descripcion;

	private String descripcion;

	@Column(name="NOMBRE_ESPECIALISTA")
	private String nombreEspecialista;

	@Column(name="NOMBRE_ESPECIALISTA")
	private String nombreEspecialista;

	//bi-directional many-to-one association to Turno
	@OneToMany(mappedBy="tipoEspecialista")
	private List<Turno> turnos;

	public TipoEspecialista() {
	}

	public int getIdTipoEspecialista() {
		return this.idTipoEspecialista;
	}

	public void setIdTipoEspecialista(int idTipoEspecialista) {
		this.idTipoEspecialista = idTipoEspecialista;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombreEspecialista() {
		return this.nombreEspecialista;
	}

	public void setNombreEspecialista(String nombreEspecialista) {
		this.nombreEspecialista = nombreEspecialista;
	}

	public String getNombreEspecialista() {
		return this.nombreEspecialista;
	}

	public void setNombreEspecialista(String nombreEspecialista) {
		this.nombreEspecialista = nombreEspecialista;
	}

	public List<Turno> getTurnos() {
		return this.turnos;
	}

	public void setTurnos(List<Turno> turnos) {
		this.turnos = turnos;
	}

	public Turno addTurno(Turno turno) {
		getTurnos().add(turno);
		turno.setTipoEspecialista(this);

		return turno;
	}

	public Turno removeTurno(Turno turno) {
		getTurnos().remove(turno);
		turno.setTipoEspecialista(null);

		return turno;
	}

}