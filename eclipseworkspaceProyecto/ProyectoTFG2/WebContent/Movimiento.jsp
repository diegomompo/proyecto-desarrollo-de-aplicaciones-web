<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cajero virtual</title>
</head>
<body>
		<h3>Cuenta: ${sessionScope.cuenta.numeroCuenta} </h3>
		<h3>Saldo: ${sessionScope.cuenta.saldo} </h3>
		
		
		<table border="1">
 			<tr><th>Cantidad</th><th>Fecha</th><th>Tipo</th></tr>
 				<c:forEach var="mov" items="${sessionScope.movi}">
 					<td>${mov.cantidad}</td>
					<td>${mov.fecha}</td>
					<td>${mov.operacion}</td></tr>
 				</c:forEach>
		</table>
		<br/><br/>
		<br/><br/>
		<form action="Main?op=volviendoOpciones" method="post">
			<button type="submit" class="btn btn-primary">Volver</button>
		</form>
</body>
</body>
</html>

