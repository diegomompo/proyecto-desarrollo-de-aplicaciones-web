<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cajero virtual</title>
</head>
<body>
	<h1>Bienvenido</h1>
	<form action="Main?op=entrando" method="post">
		<div class="form-group">
			<label for="bank_c">Introduce cuenta:</label>
			<input type="number" name="cuenta_banco" class="form-control" id="bank_c">
		</div>
		<button type="submit" class="btn btn-primary">Entrar</button>
	</form>
	
		<%String msj = (String)request.getAttribute("msj");
		if(msj != null){%>
			<p><%=msj %></p>
	<%} %>
	
</body>
</html>