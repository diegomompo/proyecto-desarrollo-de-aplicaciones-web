package modelo.dao;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public class ExtraerDaoImpl extends AbstractCuentaDao implements IntExtraerDaoImpl{

	@Override
	public int ExtraerCantidadCuenta(Cuenta cuenta) {
		int filasCuenta = 0;
		
		try {
			tx.begin();
			em.merge(cuenta);
			tx.commit();
			filasCuenta = 1;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return filasCuenta;
	}

	@Override
	public int  ExtraerCantidadMovimiento(Movimiento movimiento) {
		int filasMovimiento = 0;
		
		try {
			tx.begin();
			em.persist(movimiento);
			tx.commit();
			filasMovimiento = 1;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return filasMovimiento;
	}
}



