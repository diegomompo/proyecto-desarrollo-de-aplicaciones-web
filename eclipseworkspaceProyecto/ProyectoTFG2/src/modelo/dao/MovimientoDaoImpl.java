package modelo.dao;

import java.util.List;

import modelo.beans.Cuenta;
import modelo.beans.Movimiento;

public class MovimientoDaoImpl extends AbstractCuentaDao implements IntMovimientoDaoImpl{

	public List<Movimiento> findByCuenta(int numCuenta) {
		sql = "select m from Movimiento m where m.cuenta.numeroCuenta = :id_cuenta";
		query = em.createQuery(sql);
		query.setParameter("id_cuenta", numCuenta);
		return query.getResultList();
	}
}
